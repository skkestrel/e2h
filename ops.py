from typing import List, Tuple, Iterable, Set, Optional, Any, Dict
import itertools
import lattice
import numpy as np

pauli_indices = [1, 2, 3]
pauli_matrices = np.zeros((4, 2, 2), dtype=complex) # I, sx, sy, sz
pauli_matrices[0, :, :] = np.array([[1, 0], [0, 1]])
pauli_matrices[1, :, :] = np.array([[0, 1], [1, 0]])
pauli_matrices[2, :, :] = np.array([[0,-1j], [1j, 0]])
pauli_matrices[3, :, :] = np.array([[1, 0], [0, -1]])

Ixyz = "Ixyz"


SpinSite = Tuple[int, int] # site index, pauli index
LocalSpinOperator = List[SpinSite]

class SymmetrizedSpinOperator:
    name: str
    tags: dict
    terms: List[LocalSpinOperator]
    coefs: List[float]

    def __init__(self, name, terms):
        self.name = name
        self.tags = {}
        self.terms = terms
        self.coefs = [1.] * len(terms)

    def __str__(self):
        result = ""
        for index, term in enumerate(self.terms):
            for site in term:
                result += "s{0}({1})".format(Ixyz[site[1]], site[0])

            if index != len(self.terms) - 1:
                result += " + "

        return result

    def __repr__(self):
        return self.name

# includes information about partition number
RotatedSpinSite = Tuple[int, int, int] # relative site index (in the current site list), pauli index, partition number
LocalRotatedSpinOperator = List[RotatedSpinSite]

def symmetrized_operators(lattice: lattice.SquareLattice, n_point: int, dist: int, symmetries: List[str]) -> List[SymmetrizedSpinOperator]:
    """
    Returns a list of symmetrized operators up to n_point point operators.

    n_point: for example, 2 means only include up to 2 point operators. 3-point operators and above are currently not supported
    dist: the maximum interaction distance, in terms of nearest neighbour ordering. e.g., dist=2 means only take up to second nearest neighbour interactions
    symmetries: a list of point group symmetries that the operators should obey, such as "rot", "trans_x", "trans_y", "refl_x", "refl_y"
    """

    def offset_group_str(grp):
        return " ".join(("({0},{1})".format(*offset) for offset in grp))


    if n_point > 2:
        raise NotImplementedError()
    if n_point == 0:
        return []

    site_groups = lattice.symmetrized_group_indices(symmetries)

    # single site operators
    ops = []
    if n_point >= 1:
        for sym_group in site_groups:
            for pauli in pauli_indices:
                terms = []

                for site_index in sym_group:
                    local_op = [(site_index, pauli, 1)]
                    terms.append(local_op)

                name = "1-site {0}".format(Ixyz[pauli])
                op = SymmetrizedSpinOperator(name, terms)
                op.tags["one_site"] = True
                ops.append(op)

    # two site operators
    if n_point >= 2:
        # ii operators (xx, yy, zz)
        for offset_group in lattice.two_point_symmetrized_group_offsets(dist, symmetries, False):
            for sym_group in site_groups:
                if 'su2' not in symmetries: # no SU2
                    for pauli in pauli_indices:
                        terms = []
                        for offset in offset_group:
                            for lattice_site in sym_group:
                                shifted_index = lattice.offset_site_index(lattice_site, offset)
                                if shifted_index is not None:
                                    local_op = [(lattice_site, pauli, 1), (shifted_index, pauli, 1)]
                                    terms.append(local_op)

                        if len(terms) > 0:
                            name = '2-site {0}{0} {1}'.format(Ixyz[pauli], offset_group_str(offset_group))
                            op = SymmetrizedSpinOperator(name, terms)

                            if abs(offset[0]) + abs(offset[1]) == 1:
                                op.tags['J1'] = True
                            if abs(offset[0]) == 1 and abs(offset[1]) == 1:
                                op.tags['J2'] = True

                            op.tags['dist'] = abs(offset[0]) + abs(offset[1])

                            op.tags['two_site'] = True
                            ops.append(op)
                else: # SU2
                    terms = []
                    for pauli in pauli_indices:
                        for offset in offset_group:
                            for lattice_site in sym_group:
                                shifted_index = lattice.offset_site_index(lattice_site, offset)
                                if shifted_index is not None:
                                    local_op = [(lattice_site, pauli, 1), (shifted_index, pauli, 1)]
                                    terms.append(local_op)

                    if len(terms) > 0:
                        name = '2-site SU2 {0}'.format(offset_group_str(offset_group))
                        op = SymmetrizedSpinOperator(name, terms)

                        if abs(offset[0]) + abs(offset[1]) == 1:
                            op.tags['J1'] = True
                        if abs(offset[0]) == 1 and abs(offset[1]) == 1:
                            op.tags['J2'] = True

                        op.tags['dist'] = abs(offset[0]) + abs(offset[1])

                        op.tags['two_site'] = True
                        ops.append(op)

        # ij operators (xy, yz, yz)
        if 'su2' not in symmetries:
            for offset_group in lattice.two_point_symmetrized_group_offsets(dist, symmetries, True):
                for sym_group in site_groups:
                    for pauli1 in pauli_indices:
                        for pauli2 in pauli_indices:
                            if pauli2 <= pauli1: continue

                            terms = []
                            for offset in offset_group:
                                for lattice_site in sym_group:
                                    shifted_index = lattice.offset_site_index(lattice_site, offset)
                                    if shifted_index is not None:
                                        local_op = [(lattice_site, pauli1, 1), (shifted_index, pauli2, 1)]
                                        terms.append(local_op)

                            if len(terms) > 0:
                                name = '2-site {0}{1} {2}'.format(Ixyz[pauli1], Ixyz[pauli2], offset_group_str(offset_group))
                                op = SymmetrizedSpinOperator(name, terms)
                                op.tags['unphysical'] = True
                                if pauli2 == 3 and pauli1 < 3:
                                    # xz and yz - unphysical operators
                                    op.tags['sz_disallowed'] = True

                                op.tags['dist'] = abs(offset[0]) + abs(offset[1])

                                ops.append(op)



    return ops


def canonical_operator_order(op: LocalSpinOperator, rotate_sites: Optional[Set[int]]=None) -> Tuple[LocalRotatedSpinOperator, List[int]]:
    """
    Returns the canonical operator order for a local spin operator by commuting operators at different sites.
    The canonical order has all operators at the same sites being adjacent to each other, i.e.

    sx(53) * sx(32) * sz(53) * sy(64)
    becomes
    sx(0) * sz(0) * sx(1) * sy(2)

    in the canonical ordering.

    Returns two values: one, the operator list using the reduced indices, as shown above.
    Two, the list of sites that are mapped to by the reduced indices.

    In the above example, the returned values would be
    [(0, sx), [0, sz], [1, sx], [2, sy]]
    and
    [53, 32, 64]


    rotate_sites: site indices in this set should have rotated spin (i.e. flip Sz)
    """
    sites = []

    # build the same list, but with sites indexed by their position in the sites array instead of the actual site index
    indexed_sites = [None] * len(op)

    if rotate_sites is None:
        for index, spin_site in enumerate(op):
            if spin_site[0] not in sites:
                sites.append(spin_site[0])

            indexed_sites[index] = (sites.index(spin_site[0]), spin_site[1], 0)
    else:
        for index, spin_site in enumerate(op):
            if spin_site[0] not in sites:
                sites.append(spin_site[0])

            indexed_sites[index] = (sites.index(spin_site[0]), spin_site[1], spin_site[0] in rotate_sites)

    # sort by the position in the sites array
    indexed_sites.sort(key=lambda op: op[0])

    return indexed_sites, sites


def operator_representation(op: LocalRotatedSpinOperator, project=True, hc=True) -> np.ndarray:
    """
    Returns the matrix representation of an operator, along with the list of sites that the representation acts on.
    The input operator should be in canonical order.
    """

    if len(op) == 0:
        return np.array([])

    site_list = [op[0][0]]
    rep = 1
    cur_subspace = pauli_matrices[0] # id

    for spin_site in op:
        if site_list[-1] != spin_site[0]:
            # moving to a new spin site?
            # reset the current spin site's subspace and take tensor product
            # this isn't triggered on the very first spin site
            rep = np.kron(rep, cur_subspace)
            cur_subspace = pauli_matrices[0]
            site_list.append(spin_site[0])

        sign = 1
        if 1 <= spin_site[1] <= 2 and spin_site[2] == 1: # rotate x and y operators
            sign = -1

        cur_subspace = np.matmul(cur_subspace, sign * pauli_matrices[spin_site[1]])

    rep = np.kron(rep, cur_subspace)

    # project onto fixed Sz subspace
    if project:
        rep = project_fixed_sz(rep)

    # + h.c.
    if hc:
        rep = (rep + rep.conj().T) / 2

    return rep


def project_fixed_sz(op: np.ndarray) -> np.ndarray:
    """
    Projects a matrix operator in Sz basis onto the fixed sz subspace,
    i.e. all matrix elements are zero except for those which preserve total Sz.
    """

    def bitcount(n):
        """
        returns number of set bits in an integer
        """
        count = 0
        while n > 0:
            if (n & 1 == 1): count += 1
            n >>= 1

        return count

    if op.shape[0] != op.shape[1]:
        raise ValueError()

    cpy = op.copy()

    # total sz by index in the basis
    total_sz = [bitcount(x) for x in range(op.shape[0])]

    for i in range(cpy.shape[0]):
        for j in range(cpy.shape[0]):
            if total_sz[i] != total_sz[j]:
                cpy[i, j] = 0

    return cpy


def symmetrized_operator_product(ops: List[SymmetrizedSpinOperator]) -> Iterable[LocalSpinOperator]:
    """
    Returns the terms that comprise the product of sums of operators, i.e.
    (a1 + a2 + a3 + ...) * (b1 + b2 + b3 + ...) = a1 * b1 + a1 * b2 + ... + a2 * b1 + a2 * b2 + ... + an * bn
    """
    indexes = [0] * len(ops)
    limits = [0] * len(ops)
    
    for index, op in enumerate(ops):
        limits[index] = len(op.terms)

    while True:
        temp_list = []
        coef = 1.
        for index, op in enumerate(ops):
            temp_list += op.terms[indexes[index]]
            coef *= op.coefs[indexes[index]]

        yield temp_list, coef

        # increment
        for op_index in reversed(range(len(ops))):
            if indexes[op_index] == limits[op_index] - 1:
                # move to next most significant digit
                indexes[op_index] = 0
            else:
                indexes[op_index] += 1
                break
        else:
            break


def wf_to_str(hi, wf, mode="polar"):
    strs = []
    for i in range(hi.n_states):
        if abs(wf[i]) < 1e-4:
            continue
            
        ket = "".join(("↑" if dir > 0 else "↓" for dir in hi.number_to_state(i)))

        if mode == "cart":
            strs.append("({0:.4f}) |{1}>".format(wf[i], ket))
        elif mode == "polar":
            strs.append("{0:.4f} e^({1:.4f} iπ) |{2}>".format(np.abs(wf[i]), np.angle(wf[i]) / np.pi, ket))

    return strs


def eig_to_ham(obs, n_op, perturb=0, relative=True):
    """
    obs: dict with keys like 1 and 0,1 and so on

    the operators are indexed by a number ranging from 0 to n_op

    returns: eigval, eigvec, Q
    """

    Q = np.zeros((n_op, n_op), dtype=float) # covariance matrix
    R = np.random.normal(0, perturb, size=Q.shape)

    for i1 in range(n_op):
        for i2 in range(i1, n_op):
            op1 = str(i1)
            op2 = str(i2)

            val1 = obs[op1]['Mean']
            val2 = obs[op2]['Mean']

            prod = op1 + "," + op2
            val_prod = obs[prod]['Mean'] # This is Re(<O_{i1} O_{i2}>), therefore symmetric

            Q[i1, i2] = val_prod - val1 * val2
            if relative:
                Q[i1, i2] *= 1 + R[i1, i2]
            else:
                Q[i1, i2] += R[i1, i2]
            Q[i2, i1] = Q[i1, i2]

    val, vec = np.linalg.eigh(Q)
    argsort = np.argsort(val)

    return val[argsort], vec[:, argsort], Q


# symmetrizes a vector rotationally with the given character irrep
def symmetrize_wf_c4_t(hi, lattice, wf, c4_character, t_character):
    def rotate_state(state):
        new_state = [0] * len(state)
        for i in range(len(state)):
            coords = lattice.site_coords(i)
            new_site = lattice.site_index((lattice.len_x - coords[1] - 1, coords[0]))

            new_state[new_site] = state[i]

        return new_state


    trev_wf = np.zeros_like(wf)
    for i in range(len(wf)):
        flipped = hi.state_to_number(-hi.number_to_state(i))
        trev_wf[flipped] = np.conj(wf[i])

    wf = wf + t_character * trev_wf

    rotated_wf1 = np.zeros_like(wf)
    rotated_wf2 = np.zeros_like(wf)
    rotated_wf3 = np.zeros_like(wf)
    for i in range(len(wf)):
        rotated = hi.state_to_number(rotate_state(hi.number_to_state(i)))
        rotated_wf1[rotated] = wf[i]
        rotated = hi.state_to_number(rotate_state(hi.number_to_state(rotated)))
        rotated_wf2[rotated] = wf[i]
        rotated = hi.state_to_number(rotate_state(hi.number_to_state(rotated)))
        rotated_wf3[rotated] = wf[i]

    c = c4_character
    wf = wf + c * rotated_wf1 + c * c * rotated_wf2 + c * c * c * rotated_wf3

    wf /= np.linalg.norm(wf)
    return wf


def unrotate_wf(cfg, hi, lattice, wf):
    rotate_sites = lattice.rotated_sites(cfg['meas_wf_rotation'])

    for i in range(len(wf)):
        state = hi.number_to_state(i)

        num_up = 0
        for j in range(len(state)):
            if j in rotate_sites and state[j] > 0:
                num_up += 1

        if num_up % 2 == 1:
            wf[i] *= -1


def braket_variance(Q, vec):
    """
    computes the variance of the hamiltonian defined by vec by computing:
    <vec|Q|vec>
    """

    return np.vdot(vec, np.matmul(Q, vec))


def perp_ham(h0, h):
    """
    given the exact hamiltonian h0, finds the perpendicular part of h with respect to h0
    """

    norm_h0 = np.linalg.norm(h0)

    # project h onto the ray defined by h0, and flip so that they are pointing in same dir
    scalar_projection = abs(np.dot(h, h0) / norm_h0)

    # find the rejection of h onto h0
    rejection = h - scalar_projection * h0

    # scale the rejection up so that the projection has the same length as h0
    perp = rejection * norm_h0 / scalar_projection
    
    return perp


def perp_ham_subspace(h0, eigenval, eigenvec, tolerance=1e-14, n=None):
    """
    projects h0 onto the subspace spanned by the smallest eigenvalues of the eigensystem
    and returns the rejection of h0 onto that subspace
    """
    smallest = eigenval[0]
    proj = np.zeros_like(h0)

    for i in range(len(eigenval)):
        if (n is not None and i >= n) or (n is None and eigenval[i] > smallest + tolerance):
            break

        proj += np.dot(h0, eigenvec[:, i]) * eigenvec[:, i]

    return h0 - proj

def proj_ham_subspace(h0, eigenval, eigenvec, tolerance=1e-14, n=None):
    """
    projects h0 onto the subspace spanned by the smallest eigenvalues of the eigensystem
    """
    smallest = eigenval[0]
    proj = np.zeros_like(h0)

    for i in range(len(eigenval)):
        if (n is not None and i >= n) or (n is None and eigenval[i] > smallest + tolerance):
            break

        proj += np.dot(h0, eigenvec[:, i]) * eigenvec[:, i]

    return proj
