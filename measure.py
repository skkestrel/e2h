from mpi4py import MPI
import netket as nk
import numpy as np
import itertools
import sys
import os

import lattice
import ops
import nkutil

J2 = 0

lat = lattice.SquareLattice([4, 1], [True, False])
operators = ops.symmetrized_operators(lat, 2, 1, [])

print("{0} operators".format(len(operators)))

# simulate the model
graph = nkutil.convert_lattice(lat, 2)
hi = nk.hilbert.Spin(s=0.5, total_sz=0.0, graph=graph)
ham = nkutil.j1j2_ham(hi, J2)

n_sites = lat.len_x * lat.len_y
layers = (nk.layer.FullyConnected(input_size=n_sites, output_size=2 * n_sites, use_bias=True),
          nk.layer.Lncosh(input_size=2 * n_sites),
          nk.layer.SumOutput(input_size=2 * n_sites)) 
for layer in layers:
    layer.init_random_parameters(seed=1234, sigma=0.01)

ffnn = nk.machine.FFNN(hi, layers)

# Preserve total magnetization
sa = nk.sampler.MetropolisExchange(machine=ffnn, graph=graph)
op = nk.optimizer.Sgd(learning_rate=0.05)
gs = nk.variational.Vmc(
    hamiltonian=ham,
    sampler=sa,
    optimizer=op,
    n_samples=1000,
    diag_shift=0.1,
    method="Sr",
)

print("train")
for i in gs.iter(100):
    pass
print("ground state energy: {0}".format(gs.get_observable_stats()["Energy"]["Mean"]))

print("adding observables")
ops.add_observables(hi, gs, operators)

print("measuring")
gs.run(output_prefix="test", n_iter=1)
print("finished")

exact = nk.exact.lanczos_ed(ham, first_n=1, compute_eigenvectors=True)
print("exact gs energy: {0}".format(exact.eigenvalues[0]))
