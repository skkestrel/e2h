import base64
import hashlib

default_cfg = {
            # number of MPI threads to use for training and MC

            'mpi_threads': 1,
            # the ID of the current process
            'threading_index': (None, int),
            # total number of processes (operators split between different cores)
            'threading_total': (None, int),

            # these settings affect the machine and mc measurements
            # seed the machine rng
            'machine_seed': 1234,
            # STDEV of initial values
            'machine_sigma': 0.1,

            # type of machine: fc (fully connected), rbms (RBM, symmetrized), j1j2_exact (j1j2 CNN)
            'machine_type': 'fc',
            # parameters for machines
            'machine_fc_hidden_factor': 2,
            'machine_rbms_alpha': 2,
            # specify file to load parameters from
            'machine_from_file': (None, str),
            # unused
            'machine_j1j2_c4': 0,
            'machine_j1j2_rot_type': 'neel',

            # number of exact eigenstates to calculate (if exact mode on)
            'exact_eigens': (None, int),

            # these settings affect the machine and mc measurements
            # seed for MC sampler
            'sampler_seed': 4321,

            # these settings affect everything
            # load geometry from file
            'geom_from_file': (None, str),
            # lattice side lengths
            'geom_len_x': 3,
            'geom_len_y': 4,
            # periodic boundary conditions?
            'geom_pb_x': True,
            'geom_pb_y': True,
            # use rotational symmetry?
            'geom_rot_sym': False,

            # these settings affect everything
            # set J2 value of the hamiltonian
            'ham_j2': 0.0,
            # spin rotation? none/neel/striped
            'ham_rotation': 'none',

            # these settings affect all measurements
            # what symmetries to use for operators
            'ops_symmetries': 'none',
            # which operators to discard
            'ops_discard': 'one_site',
            # distance of two-body operators
            'ops_dist': 2,
            # parameter to tweak how anisotropy opreators are included
            'ops_only_aniso': 0,

            # these settings affect the machine and mc measurements
            # settings for optimizer (training the NN)
            'opt_discard_samples': 200,
            'opt_learning_rate': 0.05,
            'opt_samples': 1500,
            'opt_diag_shift': 0.1,
            'opt_method': 'Sr',
            'opt_iters': 300,
            'opt_batches': 10,

            # these settings affect mc measurements
            # settings for MC measurements on the already-trained optimizer
            'meas_batches': 1,
            'meas_chains': 16,
            'meas_discard_samples': 2400,
            'meas_samples': 24000,
            'meas_full_sample': False,
            'meas_full_sample_sym_c4': False,
            'meas_full_sample_sym_t': False,
            'meas_wf_rotation': 'none',

            'output_file': (None, str)
        }


def all_options():
    return default_cfg.keys()


def option_exists(name):
    return name in default_cfg


def default():
    cfg = default_cfg.copy()
    for key in cfg.keys():
        if isinstance(cfg[key], tuple):
            cfg[key] = cfg[key][0]
    return cfg


def option_type(name):
    if name not in default_cfg:
        raise ValueError()

    if not isinstance(default_cfg[name], tuple):
        return type(default_cfg[name])
    else:
        return default_cfg[name][1]


def fix(cfg):
    if cfg['machine_from_file'] is not None:
        cfg['machine_seed'] = None
        cfg['machine_sigma'] = None
        cfg['machine_hidden_factor'] = None

        for key in cfg.keys():
            if key.startswith('opt_'):
                cfg[key] = None

    if cfg['machine_type'] != 'fc':
        cfg['machine_fc_hidden_factor'] = None

    if cfg['machine_type'] != 'rbms':
        cfg['machine_rbms_alpha'] = None

    if cfg['machine_type'] != 'j1j2':
        cfg['machine_j1j2_c4'] = None
        cfg['machine_j1j2_rot_type'] = None

    if cfg['meas_full_sample']:
        cfg['meas_samples'] = None
        cfg['meas_discard_samples'] = None


def hash(cfg):
    hasher = hashlib.sha256()
    hasher.update(repr(tuple(sorted(cfg.items()))).encode())
    return base64.b32encode(hasher.digest()).decode()[:-4].lower()


def parse_option(key, value):
    if value == 'None':
        return None

    typ = option_type(key)
    if typ == int:
        return int(value)
    elif typ == float:
        return float(value)
    elif typ == str:
        return value
    elif typ == bool:
        return bool(int(value))


def repr_option(key, value):
    if value == None:
        return 'None'

    typ = option_type(key)
    if typ == int:
        return str(value)
    elif typ == float:
        return str(value)
    elif typ == str:
        return value
    elif typ == bool:
        return str(int(value))
