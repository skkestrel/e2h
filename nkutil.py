from typing import Iterable, Dict

import numpy as np
from mpi4py import MPI
import ops
from ops import LocalSpinOperator

import netket as nk

op_reps = {}


def convert_lattice(lattice, rotate: bool=False, nbr: int=2) -> nk.graph.Graph:
    """
    nbr: the number of nearest neighbours to include in the nk.hilbert graph, eg 2 -> up to next nearest neighbours
            * graph color 1 -> x nearest neighbor
            * graph color 2 -> y nearest neighbor
            * graph color 3 -> next nearest neighbor
    rotate: include rotation automorphisms?
    """
    if nbr > 3:
        raise ValueError("not implemented")

    if rotate and lattice.len_x != lattice.len_y:
        raise ValueError("can only use rotation automorphism if square lattice")

    edges = []

    if nbr >= 1:
        for index in lattice.iter_site_indices():
            a = lattice.offset_site_index(index, [1, 0])
            if a is not None:
                edges.append((index, a, 1))
            a = lattice.offset_site_index(index, [0, 1])
            if a is not None:
                edges.append((index, a, 2))

    if nbr >= 2:
        for index in lattice.iter_site_indices():
            a = lattice.offset_site_index(index, [1, 1])
            if a is not None:
                edges.append((index, a, 3))
            a = lattice.offset_site_index(index, [1, -1])
            if a is not None:
                edges.append((index, a, 3))

    if nbr >= 3:
        for index in lattice.iter_site_indices():
            coords = lattice.site_coords(index)
            if lattice.len_x % 2 == 0:
                if coords[0] >= (lattice.len_x // 2):
                    continue
            if lattice.len_y % 2 == 0:
                if coords[1] >= (lattice.len_y // 2):
                    continue

            a = lattice.offset_site_index(index, [0, 2])
            if a is not None:
                edges.append((index, a, 4))
            a = lattice.offset_site_index(index, [2, 0])
            if a is not None:
                edges.append((index, a, 4))

    automorphisms = []
    x_range = range(lattice.len_x) if lattice.periodic_x else [0]
    y_range = range(lattice.len_y) if lattice.periodic_y else [0]
    
    for x in x_range:
        for y in y_range:
            l = []
            for index in lattice.iter_site_indices():
                l.append(lattice.offset_site_index(index, [x, y]))
            automorphisms.append(l)

    rotated_automorphisms = []
    for automorph in automorphisms:
        rotated_automorphisms.append(automorph)

        if rotate:
            new_morph = []
            new_morph1 = []
            new_morph2 = []
            for index in automorph:
                (x, y) = lattice.site_coords(index)
                new_morph.append(lattice.site_index((lattice.len_x - 1 - y, x)))
                new_morph1.append(lattice.site_index((lattice.len_x - 1 - y, lattice.len_y - 1 - x)))
                new_morph2.append(lattice.site_index((y, lattice.len_y - 1 - x)))
            rotated_automorphisms.append(new_morph)
            rotated_automorphisms.append(new_morph1)
            rotated_automorphisms.append(new_morph2)

    return nk.graph.CustomGraph(edges, rotated_automorphisms)


def convert_operator(hi, terms: Iterable[LocalSpinOperator], coefficients: Iterable[float], rotate_sites, project=True, hc=True) -> nk.operator.LocalOperator:
    """
    Return a nk operator from a list of terms
    """

    mats = []
    sites = []
    for op, coef in zip(terms, coefficients):
        # get the canonical operator order
        # the variable key has the site indexes replaced with normalized indexes, e.g. [5, 6, 35, 5] -> [0, 1, 2, 0]
        # this is because all operators with the same site topology share the same matrix representation
        key, sites_ = ops.canonical_operator_order(op, rotate_sites)
        key = tuple(key)

        if key not in op_reps:
            # get the matrix representation of the operator
            op_reps[key] = ops.operator_representation(list(key), project, hc)

        if np.all(op_reps[key] == 0):
            # zero matrix - don't need to measure this
            pass
        else:
            mats.append(coef * op_reps[key])
            sites.append(sites_)

    if len(mats) == 0:
        # identically nil - operator has no terms
        return None
    else:
        return nk.operator.LocalOperator(hi, mats, sites)


def reconstructed_ham(hi, rotate_sites, operators, vec):
    mats = []
    sites = []
    for op, coef in zip(operators, vec):
        for terms, termcoef in zip(op.terms, op.coefs):
            key, sites_ = ops.canonical_operator_order(terms, rotate_sites)
            rep = ops.operator_representation(list(key), True, True)

            if np.all(rep == 0):
                pass
            else:
                mats.append(termcoef * coef * rep)
                sites.append(sites_)

    return nk.operator.LocalOperator(hi, mats, sites)

def reconstructed_ham_sq(hi, rotate_sites, operators, vec):
    mats = []
    sites = []

    new_op_terms = []
    new_op_coefs = []
    for op, coef in zip(operators, vec):
        new_op_terms += op.terms
        new_coefs = op.coefs.copy()
        for i in range(len(new_coefs)):
            new_coefs[i] *= coef

        new_op_coefs += new_coefs

    new_op = ops.SymmetrizedSpinOperator('Recons Ham', new_op_terms)
    new_op.coefs = new_op_coefs

    product = list(ops.symmetrized_operator_product([new_op, new_op]))
    op = convert_operator(hi, [x[0] for x in product], [x[1] for x in product], rotate_sites)
    return op


def j1j2_ham(hi, j2, rotation, j3=None):
    """
    colors:
        1: nearest neighbour x, 2: nearest neighbour y, 3: next nearest

    striped rotation is striped along x axis
    """
    sz = [[1, 0], [0, -1]]
    sy = [[0, -1j], [1j, 0]]
    sx = [[0, 1], [1, 0]]
    coupling = np.kron(sx, sx) + np.kron(sy, sy) + np.kron(sz, sz)
    inv_coupling = -np.kron(sx, sx) - np.kron(sy, sy) + np.kron(sz, sz)

    if rotation.lower() == 'neel':
        n1x_coupling = inv_coupling
        n1y_coupling = inv_coupling
        n2_coupling = coupling
    elif rotation.lower() == 'striped':
        n1x_coupling = coupling
        n1y_coupling = inv_coupling
        n2_coupling = inv_coupling
    else:
        n1x_coupling = coupling
        n1y_coupling = coupling
        n2_coupling = coupling

    j1 = 1
    if j2 > 1000000:
        j2 = 1
        j1 = 0

    bondops = [n1x_coupling * j1, n1y_coupling * j1, n2_coupling * j2]
    bondops_colors = [1, 2, 3]
    if j3:
        bondops.append(coupling * j3)
        bondops_colors.append(4)

    return nk.operator.GraphOperator(hi, bondops=bondops, bondops_colors=bondops_colors)


def convert_observables(hi, operators, rotate_sites, project=True, process_index=0, total_processes=1) -> Dict[str, nk.operator.LocalOperator]:
    """
    add operators and propucts of operators to a dict
    """
    observables = {}

    obs_index = 0

    for a_ind in range(len(operators)):
        # single operators
        a_op = operators[a_ind]
        if obs_index % total_processes == process_index:
            name = "{0}".format(a_ind)
            op = convert_operator(hi, a_op.terms, a_op.coefs, rotate_sites, project=project)
            observables[name] = op

        obs_index += 1

        for b_ind in range(a_ind, len(operators)):
            # operator products
            if obs_index % total_processes == process_index:
                b_op = operators[b_ind]
                product = list(ops.symmetrized_operator_product([a_op, b_op]))
                name = "{0},{1}".format(a_ind, b_ind)
                op = convert_operator(hi, [x[0] for x in product], [x[1] for x in product], rotate_sites, project=project)
                observables[name] = op

            obs_index += 1

    return observables
