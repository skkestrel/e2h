# basic execution

python3 e2h.py 4x4 [OPTIONS...]
the options you can specifiy are listed in config.py

for example, you can do this to do WF2H on a 4x4 lattice, RBMS pre-trained machine loaded from file
operators are J1 and J2
data = main(['python3 e2h.py', '4x4', '-machine_type', 'rbms', '-machine_rbms_alpha', '10', '-ops_dist', '2', '-ham_j2', j2, '-machine_from_file', 'model.json', '-meas_full_sample', '1', '-meas_full_sample_sym_c4', '1', '-meas_full_sample_sym_t', '1', '-ops_symmetries', 'trans_x,trans_y,rot'])

# multicore running

sudo apt install cmake liblapack-dev libblas-dev libopenmpi-dev


# building the dockerfile
follow instructions here

https://docs.docker.com/install/linux/docker-ce/ubuntu/

if on WSL, install docker on host windows machine and expose the port, then run
```echo "export DOCKER_HOST=tcp://localhost:2375" >> ~/.bashrc && source ~/.bashrc```
in WSL

(may need to disable and enable Hyper-V in windows features to get port forwarding to work)

# running deployment scripts
do this and source the identity file first (also need to do sudo apt install python3-openstackclient)

https://www.cac.cornell.edu/wiki/index.php?title=Using_Openstack_CLI_Client

may need to follow steps here

https://token2shell.com/howto/x410/setting-up-wsl-for-linux-gui-apps/

specifically ```sudo dbus-uuidgen --ensure```

# deploying
first get your rc file from https://redcloud.cac.cornell.edu/dashboard/project/api_access/
click download openstack RC file and use identity v3

then source the file (this sets environment variables)

now you can call
```python3 deploy.py exec 3x4 ...```
this discovers all instances with a name that starts with e2h and finds out their number of cores

the first parameter exec tells the program that this is a multicore run, so it deploys the same number of processes per instance as the number of instances it has, and sets the threading_total and threading_index parameters properly
