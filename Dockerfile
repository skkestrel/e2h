FROM python:rc

SHELL ["/bin/bash", "-c"]

USER root

# install dependencies and useful tools
RUN apt-get update -y && apt-get install -y \
    sudo \
    cmake \
    liblapack-dev \
    libblas-dev \
    libopenmpi-dev \
    unzip \
    man \
    bash-completion \
    nano \ 
    vim \ 
    less \
    htop \
    tmux \
    python3-dev 

# set up user
RUN useradd -mr -G sudo dev 
RUN passwd -d dev
RUN echo "dev  ALL=(ALL)       NOPASSWD: ALL"  >> /etc/sudoers

RUN chown -R dev /home/dev
WORKDIR /home/dev

RUN pip3 install scikit-build \
    matplotlib \
    mpi4py \
    cmake \
    pybind11

# install netket
RUN git clone https://github.com/kchoo1118/netket.git
WORKDIR /home/dev/netket
RUN git checkout J1J2-V2.1
RUN pip3 install .

COPY ./ /home/dev/src/
RUN chown -R dev /home/dev

WORKDIR /home/dev/src
USER dev
ENTRYPOINT ["python3", "e2h.py"]
