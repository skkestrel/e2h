#!/usr/bin/env python
# coding: utf-8

from mpi4py import MPI
import netket as nk
import numpy as np

import shutil
import time
import json
import itertools
import sys
import os
import glob

import lattice
import ops
import nkutil
import config

comm = MPI.COMM_WORLD

def master_thread():
    return nk.MPI.rank() == 0

disable_print = False

def print_(cfg, *x):
    """
    print to both stdout and optionally a file
    """
    if disable_print:
        return

    if not master_thread():
        return

    print('***', *x)
    f = 'stdout'
    if cfg['threading_index'] is not None:
        f += str(cfg['threading_index'])
    with open(f, 'a') as f:
        f.write(' '.join([str(a) for a in x]) + '\n')


def save_run_data(run_data, path):
    """
    save run data (including cfg) to file
    """
    with open(path, 'w') as f:
        f.write(json.dumps(run_data, indent=1))


def load_run(partial_name, relative_path=False):
    """
    load run from runs/ directory if relative_path=False
    load from relative path if relative_path=True

    supports partial filenames
    """
    if relative_path:
        with open(partial_name, 'r') as f:
            run_data = json.load(f)
    else:
        matches = glob.glob('runs/' + partial_name.lower() + '*.json')
        if len(matches) > 1:
            print_(cfg, 'more than one run with that name')
            sys.exit(1)
        if len(matches) == 0:
            print_(cfg, 'no such run exists')
            sys.exit(1)
        with open(matches[0], 'r') as f:
            run_data = json.load(f)

    cfg = run_data['cfg']

    return cfg, run_data


def prepare_hilbert(cfg):
    """
    prepare netket objects, such as lattice, operators (to be used in WF reconstruction)
    """
    lat = lattice.SquareLattice([cfg['geom_len_x'], cfg['geom_len_y']], [cfg['geom_pb_x'], cfg['geom_pb_y']])
    operators = ops.symmetrized_operators(lat, n_point=2, dist=cfg['ops_dist'], symmetries=cfg['ops_symmetries'].split(',')) 

    for remove in cfg['ops_discard'].split(','):
        operators = [op for op in operators if remove not in op.tags]

    if int(cfg['ops_dist']) >= 3:
        # rewrite operators as (J1J2, J3) if including the J3 operators
        heis_operators = ops.symmetrized_operators(lat, n_point=2, dist=2, symmetries='trans_x,trans_y,rot,su2')
        heis_operators = [op for op in heis_operators if 'one_site' not in op.tags]
        n_j1terms = len(heis_operators[0].terms)
        n_j2terms = len(heis_operators[1].terms)
        j1j2_operator = ops.SymmetrizedSpinOperator('J1J2', heis_operators[0].terms + heis_operators[1].terms)
        j1j2_operator.coefs = [1.] * n_j1terms + [cfg['ham_j2']] * n_j2terms
        j1j2_operator.tags = ["J1"]

        operators = [j1j2_operator, operators[2]]
        # operators = [heis_operators[0], heis_operators[1], operators[2]]

    if cfg['ops_only_aniso'] > 0:
        # rewrite operators based on value of config parameter
        heis_operators = ops.symmetrized_operators(lat, n_point=2, dist=2, symmetries='trans_x,trans_y,rot,su2')
        heis_operators = [op for op in heis_operators if 'one_site' not in op.tags]
        n_j1terms = len(heis_operators[0].terms)
        n_j2terms = len(heis_operators[1].terms)
        j1j2_operator = ops.SymmetrizedSpinOperator('J1J2', heis_operators[0].terms + heis_operators[1].terms)

        j1j2_operator.coefs = [1] * n_j1terms + [cfg['ham_j2']] * n_j2terms

        aniso = ops.symmetrized_operators(lat, n_point=2, dist=2, symmetries='trans_x,trans_y,rot')
        aniso = [op for op in aniso if 'one_site' not in op.tags]

        combined_aniso = ops.SymmetrizedSpinOperator('Anisotropy', aniso[2].terms + aniso[5].terms)
        combined_aniso.coefs = [1] * len(aniso[2].terms) + [cfg['ham_j2']] * len(aniso[5].terms)

        aniso_j1 = ops.SymmetrizedSpinOperator('Aniso J1', aniso[2].terms)
        aniso_j1.coefs = [1] * len(aniso[2].terms)

        aniso_j2 = ops.SymmetrizedSpinOperator('Aniso J2', aniso[5].terms)
        aniso_j2.coefs = [1] * len(aniso[5].terms)

        if cfg['ops_only_aniso'] == 1:
            # J1J2, J1 ZZ, J2 ZZ
            operators = [j1j2_operator, aniso_j1, aniso_j2]
        elif cfg['ops_only_aniso'] == 2:
            # J1J2, J1ZZ + J2ZZ
            operators = [j1j2_operator, combined_aniso]
        elif cfg['ops_only_aniso'] == 3:
            # J1, J2, J1ZZ + J2ZZ
            operators = [heis_operators[0], heis_operators[1], combined_aniso]
        elif cfg['ops_only_aniso'] == 4:
            # J1, J2, J1ZZ, J2ZZ, J3
            j3op = ops.symmetrized_operators(lat, n_point=2, dist=3, symmetries='trans_x,trans_y,rot,su2')
            print(j3op)
            operators = [heis_operators[0], heis_operators[1], aniso_j1, aniso_j2, j3op[5]]
        elif cfg['ops_only_aniso'] == 5:
            # J1, J2, J1ZZ+J2ZZ, J3
            j3op = ops.symmetrized_operators(lat, n_point=2, dist=3, symmetries='trans_x,trans_y,rot,su2')
            operators = [heis_operators[0], heis_operators[1], combined_aniso, j3op[5]]
        elif cfg['ops_only_aniso'] == 6:
            # J1J2, dJ2, J1ZZ+J2ZZ
            j3op = ops.symmetrized_operators(lat, n_point=2, dist=3, symmetries='trans_x,trans_y,rot,su2')
            operators = [j1j2_operator, heis_operators[1], combined_aniso]
        elif cfg['ops_only_aniso'] == 7:
            # J1J2, dJ2, J3
            j3op = ops.symmetrized_operators(lat, n_point=2, dist=3, symmetries='trans_x,trans_y,rot,su2')
            operators = [j1j2_operator, heis_operators[1], j3op[5]]


    print_(cfg, operators)

    if cfg['geom_from_file'] is not None:
        data = json.loads(open(cfg['geom_from_file'], 'r').read())
        graph = nk.graph.CustomGraph(data['EdgeColors'], data['Automorphisms'], data['AdjacencyList'])
        hi = nk.hilbert.Spin(s=0.5, total_sz=0.0, graph=graph)
        ham = nkutil.j1j2_ham(hi, cfg['ham_j2'], cfg['ham_rotation'])
    else:
        graph = nkutil.convert_lattice(lat, cfg['geom_rot_sym'], 2)
        hi = nk.hilbert.Spin(s=0.5, total_sz=0.0, graph=graph)
        ham = nkutil.j1j2_ham(hi, cfg['ham_j2'], cfg['ham_rotation'])

    return operators, lat, graph, hi, ham


def prepare_run_data(cfg, path, checksum):
    """
    create cfg file if not exists, else load existing run data
    """
    if os.path.exists(path) and cfg['output_file'] is None:
        try:
            with open(path, 'r') as f:
                print_(cfg, 'found existing run data')
                run_data = json.load(f)
            if frozenset(run_data['cfg'].items()) != frozenset(cfg.items()) or run_data['cfg_checksum'] != checksum:
                raise RuntimeError('cfg doesn\'t match file')

        except json.decoder.JSONDecodeError:
            print_(cfg, 'malformed json')
            # overwrite if malformed
            run_data = { 'cfg': cfg, 'cfg_checksum': checksum }
            save_run_data(run_data, path)
    else:
        run_data = { 'cfg': cfg, 'cfg_checksum': checksum }
        save_run_data(run_data, path)

    return run_data


def prepare_run_tasks(cfg, run_data, force_retrain, force_resample, force_reexact, find_machine, exact_mode, diag_only, skip_meas):
    """
    determine what tasks to do based on cfg
    """
    run_tasks = {
            'train': 'do',
            'meas': 'do',
            'diag': 'do',
            'exact': 'skip',
            'exact_meas': 'skip'
        }

    if ('wf' in run_data and not force_retrain) or find_machine or cfg['machine_from_file'] is not None:
        run_tasks['train'] = 'load'

    if exact_mode or diag_only:
        run_tasks['train'] = 'skip'

    if ('meas' in run_data and not force_retrain and not force_resample) or diag_only:
        run_tasks['meas'] = 'skip'

    if exact_mode or skip_meas:
        run_tasks['meas'] = 'skip'

    if cfg['threading_total'] is not None and not diag_only:
        run_tasks['diag'] = 'skip'

    if ('exact' not in run_data or force_reexact) and cfg['exact_eigens'] is not None:
        run_tasks['exact'] = 'do'

    if ('exact_meas' not in run_data or force_reexact) and cfg['exact_eigens'] is not None:
        run_tasks['exact_meas'] = 'do'

    return run_tasks


def prepare_nn(cfg, lat, hi, graph):
    """
    prepare NN based on specified machinetype
    """

    n_sites = lat.len_x * lat.len_y
    layers = None
    ffnn = None
    __keep_alive = None

    if cfg['machine_type'] == 'fc':
        n = cfg['machine_fc_hidden_factor']

        layers = (nk.layer.FullyConnected(input_size=n_sites, output_size=n*n_sites, use_bias=True),
                  nk.layer.Lncosh(input_size=n*n_sites),
                  nk.layer.SumOutput(input_size=n*n_sites)) 
        ffnn = nk.machine.FFNN(hi, layers)
    elif cfg['machine_type'] == 'rbms':
        if cfg['geom_len_x'] != cfg['geom_len_y']:
            print_(cfg, 'rectangular lattice not supported with rbms')

        if not cfg['geom_pb_x'] or not cfg['geom_pb_y']:
            print_(cfg, 'non periodic lattice not supported with rbms')

        ffnn = nk.machine.RbmSpinSymm(hi, alpha=cfg['machine_rbms_alpha'])
    elif cfg['machine_type'] == 'j1j2_exact':
        if lat.len_x != lat.len_y or lat.len_x != 4:
            print_(cfg, 'lattice must be 4x4')
            sys.exit(1)

        L = lat.len_x

        layers = (
            nk.layer.ConvolutionalHypercube(
                length=L,
                n_dim=2,
                input_channels=1,
                output_channels=6,
                stride=1,
                kernel_length=4,
                use_bias=True,
            ),
            nk.layer.Lncosh(input_size=6 * L * L),
            nk.layer.ConvolutionalHypercube(
                length=L,
                n_dim=2,
                input_channels=6,
                output_channels=4,
                stride=1,
                kernel_length=2,
                use_bias=True,
            ),
            nk.layer.Lncosh(input_size=4 * L * L),
            nk.layer.ConvolutionalHypercube(
                length=L,
                n_dim=2,
                input_channels=4,
                output_channels=2,
                stride=1,
                kernel_length=2,
                use_bias=True,
            ),
            nk.layer.Lncosh(input_size=2 * L * L),
            nk.layer.SumOutput(input_size=(2 * L * L)),
        )
        ffnn = nk.machine.FFNN(hi, layers)

    if cfg['machine_seed'] is not None:
        ffnn.init_random_parameters(seed=cfg['machine_seed'], sigma=cfg['machine_sigma'])

    sa = nk.sampler.MetropolisExchange(machine=ffnn, n_chains=cfg['meas_chains'])

    if cfg['opt_learning_rate'] is not None:
        opt = nk.optimizer.Sgd(learning_rate=cfg['opt_learning_rate'])
    else:
        opt = nk.optimizer.Sgd(learning_rate=0)

    return ffnn, sa, opt, __keep_alive


def train_model(cfg, run_data, ham, sa, opt, ffnn, plot_training_energy, hi):
    """
    train a model
    """
    fname = 'temp'
    if cfg['threading_total'] is not None:
        fname = 'temp{0}'.format(cfg['threading_index'])

    if cfg['machine_type'] not in ['fc', 'rbms', 'j1j2_exact']:
        print_(cfg, 'can\'t train specified machine type')
        sys.exit(1)

    gs = nk.variational.Vmc(
        hamiltonian=ham,
        sampler=sa,
        optimizer=opt,
        n_samples=cfg['opt_samples'],
        discarded_samples=cfg['opt_discard_samples'],
        diag_shift=cfg['opt_diag_shift'],
        method=cfg['opt_method'],
    )

    if master_thread():
        if plot_training_energy:
            plt.ion()
            fig, ax = plt.subplots(2)
            plt.show()


    t_start = time.time()
    training_energy = []
    training_energy_err = []
    for i in range(cfg['opt_batches']):
        gs.run(output_prefix=fname, n_iter=cfg['opt_iters'] // cfg['opt_batches'])
        training_energy.append(gs.get_observable_stats()['Energy'].mean.real)
        training_energy_err.append(gs.get_observable_stats()['Energy'].error_of_mean)
        print_(cfg, 'finished batch {0} of {1} (total time: {2}). E = {3} +- {4}'.format(i + 1, cfg['opt_batches'], time.time() - t_start, training_energy[-1], training_energy_err[-1]))

        if plot_training_energy:
            ax[0].clear()
            ax[1].clear()
            ax[0].scatter(np.arange(len(training_energy)), training_energy)
            ax[1].scatter(np.arange(len(training_energy_err)), training_energy_err)
            plt.draw()
            plt.pause(0.1)

        wf = np.zeros((hi.n_states,), dtype=complex)
        for i in range(hi.n_states):
            v = hi.number_to_state(i)
            # Sz = 0 subspace
            if np.sum(v) > -0.5 and np.sum(v) < 0.5:
                wf[i] = np.exp(ffnn.log_val(v))

        wf /= np.linalg.norm(wf)
        np.savetxt("tempwf.wf", wf)

    if master_thread():
        ffnn.save(fname + '.wf')
        with open(fname + '.wf', 'r') as f:
            run_data['wf'] = json.load(f)
            run_data['wf_energy'] = training_energy[-1]
            run_data['wf_energy_err'] = training_energy_err[-1]


def find_compatible_run(cfg):
    """
    find a run with the same options as the cfg specified
    tries to match all config parameters, except for the meas_ and ops_ settings
    """
    for run in glob.glob('runs/*.json'):
        candidate_data = None
        with open(run, 'r') as f:
            try:
                candidate_data = json.load(f)
            except json.JSONDecodeError:
                print_(cfg, 'could not decode file', run)
                continue

        if 'wf' not in candidate_data:
            continue

        cpy_cfg = cfg.copy()
        cpy_cand = candidate_data['cfg'].copy()

        for key in frozenset(list(cpy_cand.keys()) + list(cpy_cfg.keys())):
            if key.startswith('meas_') or key.startswith('ops_') or key.startswith('threading_') or key.startswith('exact_') or key.startswith('output_'):
                if key in cpy_cand:
                    del cpy_cand[key]

                if key in cpy_cfg:
                    del cpy_cfg[key]

        if frozenset(cpy_cand.items()) == frozenset(cpy_cfg.items()):
            print_(cfg, 'candidate found:', run)
            return candidate_data

    return None


def load_model(cfg, run_data, ffnn):
    """
    load a NN model from disk
    """
    fname = 'temp.wf'
    if cfg['threading_total'] is not None:
        fname = 'temp{0}.wf'.format(cfg['threading_index'])

    if cfg['machine_type'] in ['fc', 'rbms', 'j1j2_exact']:
        if cfg['machine_from_file'] is not None:
            fname = cfg['machine_from_file']
        else:
            if master_thread():
                wf_load_success = False

                if 'wf' in run_data:
                    print_(cfg, 'using existing wf')
                    # write wf data to file so ffnn can load it from file
                    with open(fname, 'w') as f:
                        f.write(json.dumps(run_data['wf']))

                    wf_load_success = True
                else:
                    print_(cfg, 'looking for existing wf')
                    candidate_data = find_compatible_run(cfg)
                    if candidate_data is not None:
                        run_data['wf_energy'] = candidate_data['wf_energy']
                        if 'wf_energy_err' in candidate_data:
                            run_data['wf_energy_err'] = candidate_data['wf_energy_err']

                        with open(fname, 'w') as f:
                            f.write(json.dumps(candidate_data['wf']))

                        wf_load_success = True
                    else:
                        print_(cfg, 'no compatible wf found')
            else: # not master_thread()
                wf_load_success = None

                wf_load_success = comm.bcast(wf_load_success, root=0)
                if not wf_load_success:
                    sys.exit(1)

        ffnn.load(fname)


def calculate_exact_gs(cfg, hi, run_data, ham):
    if not master_thread():
        raise RuntimeError()

    exact = nk.exact.lanczos_ed(ham, first_n=cfg['exact_eigens'], compute_eigenvectors=True)

    exact_wf = { 'vals': exact.eigenvalues, 'vecs': exact.eigenvectors }

    run_data['exact'] = { 'vecs': [[str(x) for x in exact_vec] for exact_vec in exact_wf['vecs']], 'vals': [np.real(exact_val) for exact_val in exact_wf['vals']] }
    return exact_wf


def extract_wavefunction(cfg, hi, ham, ffnn, lat, character=None):
    """
    extract wavefunction into a vector from NN
    optionally symmetrizing with characteristic=(rot_char, time_reversal_char)
    """
    wf = np.zeros((hi.n_states,), dtype=complex)
    for i in range(hi.n_states):
        v = hi.number_to_state(i)
        # Sz = 0 subspace
        if np.sum(v) > -0.5 and np.sum(v) < 0.5:
            wf[i] = np.exp(ffnn.log_val(v)) 
    wf /= np.linalg.norm(wf)

    if cfg['meas_wf_rotation'] != 'none':
        ops.unrotate_wf(cfg, hi, lat, wf)

    if character is None:
        print_(cfg, 'variational energy:', np.real(np.vdot(wf, ham.to_sparse() * wf)))

        best_energy = None;
        best_character = None;
        c4_characters = [1, -1, 1j, -1j] if cfg['meas_full_sample_sym_c4'] else [0]
        t_characters = [1, -1] if cfg['meas_full_sample_sym_t'] else [0]

        for c in c4_characters:
            for tc in t_characters:
                wf_ = ops.symmetrize_wf_c4_t(hi, lat, wf, c, tc)
                e = np.real(np.vdot(wf_, ham.to_sparse() * wf_))
                print_(cfg, 'energy of wf symmetrized with c4 character {0}, t character {1}: {2}'.format(c, tc, e))
                if best_energy is None or e < best_energy:
                    best_energy = e
                    best_character = (c, tc)
        wf = ops.symmetrize_wf_c4_t(hi, lat, wf, best_character[0], best_character[1])
    else:
        wf = ops.symmetrize_wf_c4_t(hi, lat, wf, character[0], character[1])
    return wf


def measure_full_sampling(cfg, run_data, hi, ffnn, operators, observables, ham, lat):
    """
    measure operators exactly by calculating
    <Psi| O |Psi>
    """
    if not master_thread():
        raise RuntimeError()

    print_(cfg, '*** extracting wavefunction...'); t_start = time.time()
    wf = extract_wavefunction(cfg, hi, ham, ffnn, lat)

    measurements = {}
    index = 0
    for key, val in observables.items():
        if val is None:
            # no observable to measure - it's zero
            measurements[key] = { 'Mean': 0 }
        else:
            expect = np.vdot(wf, val.to_sparse() * wf)
            measurements[key] = { 'Mean': np.real(expect) }

        index += 1
        if index % 20 == 0:
            print_(cfg, '{0:.2f}% done'.format(index / len(observables) * 100))

    run_data['meas'] = measurements

    if cfg['ops_only_aniso'] > 0:
        if cfg['ops_only_aniso'] > 2:
            energy = run_data['meas']['1']['Mean']
        else:
            energy = run_data['meas']['0']['Mean']
    else:
        energy = 0
        for index, op in enumerate(operators):
            if 'J1' in op.tags:
                energy += 1 * run_data['meas'][str(index)]['Mean']
            elif 'J2' in op.tags:
                energy += cfg['ham_j2'] * run_data['meas'][str(index)]['Mean']
    

    run_data['meas']['Energy'] = { 'Mean': energy }


def measure_mc2(cfg, run_data, ham, ffnn, sa, observables, run_data_path):
    fname = 'temp'
    if cfg['threading_total'] is not None:
        fname = 'temp{0}'.format(cfg['threading_index'])

    samples_per_batch = cfg['meas_samples'] // cfg['meas_batches']
    run_data['meas'] = {}

    opt = nk.optimizer.Sgd(learning_rate=0)
    sa = nk.sampler.MetropolisExchange(machine=ffnn, n_chains=cfg['meas_chains'])
    gs = nk.variational.Vmc(
        hamiltonian=ham,
        sampler=sa,
        optimizer=opt,
        n_samples=samples_per_batch * cfg['meas_chains'], # Vmc divides n_samples between chains
        discarded_samples=cfg['meas_discard_samples'],
        diag_shift=0,
        method='Gd',
    )

    zero_ops = []
    for name, op in observables.items():
        if op is not None:
            # only measure nonzero ops
            gs.add_observable(op, name)
        else:
            zero_ops.append(name)

    t_start = time.time()
    for i in range(cfg['meas_batches']):
        gs.run(output_prefix=fname, n_iter=1, write_every=1)

        if master_thread():
            print_(cfg, 'finished batch {0} of {1} (total time: {2})'.format(i + 1, cfg['meas_batches'], time.time() - t_start))
            with open(fname + '.log', 'r') as f:
                measurements = json.load(f)['Output'][0]

            for key in measurements:
                if key == 'Iteration':
                    continue
                if i == 0:
                    run_data['meas'][key] = { 'batch_means': [], 'batch_errors': [] }

                run_data['meas'][key]['batch_means'].append(measurements[key]['Mean'])
                run_data['meas'][key]['batch_errors'].append(measurements[key]['Sigma'])

            for key in zero_ops:
                if i == 0:
                    run_data['meas'][key] = { 'batch_means': [], 'batch_errors': [] }

                run_data['meas'][key]['batch_means'].append(0)
                run_data['meas'][key]['batch_errors'].append(0)

            save_run_data(run_data, run_data_path)


def measure_mc(cfg, run_data, observables, sampler):
    if not master_thread():
        return

    obs = list(observables.items())
    samples = cfg['meas_samples'] // cfg['meas_batches']

    t_start = time.time()
    run_data['meas'] = {}
    for i in range(cfg['meas_batches']):
        stats = nk.variational.estimate_expectations(
                (op for name, op in obs if op is not None),
                sampler,
                samples,
                cfg['meas_discard_samples'] if i == 0 else 0)

        if i == 0:
            for name, op in obs:
                run_data['meas'][name] = { 'batch_means': [], 'batch_errors': [] }

        for name, stat in zip((name for name, op in ops if op is not None), stats):
            run_data['meas'][name]['batch_means'].append(stat.mean.real)
            run_data['meas'][name]['batch_errors'].append(stat.error_of_mean)
        for name in (name for name, op in ops if op is None):
            run_data['meas'][name]['batch_means'].append(0)
            run_data['meas'][name]['batch_errors'].append(0)

        print_(cfg, 'finished batch {0} of {1} (total time: {2})'.format(i + 1, cfg['meas_batches'], time.time() - t_start))


def measure_exact_wf(cfg, run_data, exact_wf, observables):
    """
    make measurements on exact GS found through ED
    """
    if not master_thread():
        raise RuntimeError()

    measurements = {}
    index = 0

    for key, val in observables.items():
        if val is None:
            measurements[key] = { 'Mean': 0 }
        else:
            expect = np.vdot(exact_wf['vecs'][0], val.to_sparse() * exact_wf['vecs'][0])
            measurements[key] = { 'Mean': np.real(expect) }

        index += 1
        if index % 500 == 0:
            print_(cfg, '{0:.2f}% done'.format(index / len(observables) * 100))

    measurements['Energy'] = { 'Mean': exact_wf['vals'][0] }

    run_data['exact_meas'] = measurements


def diagonalize(cfg, hi, ham, ffnn, lat, rotate_sites, run_data, measurements, operators, print_eigens, subspace_eps, plot_rejection, plot_perturb_correlators, print_correlators):
    """
    do WF2H
    """
    if not master_thread():
        raise RuntimeError()

    data = {}
    data['cfg'] = cfg
    data['lat'] = lat
    data['hi'] = hi
    data['ham'] = ham
    data['ffnn'] = ffnn
    data['rotate_sites'] = rotate_sites
    data['run_data'] = run_data
    data['measurements'] = measurements
    data['operators'] = operators

    if 'Energy' in measurements:
        data['energy'] = measurements['Energy']['Mean']
        print_(cfg, 'measured energy:', measurements['Energy']['Mean'])

    if cfg['ops_only_aniso'] > 0:
        if cfg['ops_only_aniso'] == 1 or cfg['ops_only_aniso'] == 2 or cfg['ops_only_aniso'] == 6 or cfg['ops_only_aniso'] == 7:
            h0 = np.array([1. / (1 + cfg['ham_j2']**2)] + [0.] * (len(operators) - 1))
        elif cfg['ops_only_aniso'] == 3 or cfg['ops_only_aniso'] == 4 or cfg['ops_only_aniso'] == 5:
            h0 = np.array([1., cfg['ham_j2']] + [0.] * (len(operators) - 2))
            h0 = h0 / np.linalg.norm(h0)
    else:
        h0 = np.zeros((len(operators),))
        for i in range(len(operators)):
            if "J1" in operators[i].tags:
                h0[i] = 1
            if "J2" in operators[i].tags:
                h0[i] = cfg['ham_j2']
        h0 = h0 / np.linalg.norm(h0)


    if plot_perturb_correlators:
        X = np.arange(1, min(50, len(operators)))
        Y = np.linspace(0, 0.2, 30)

        Z = np.zeros((len(X), len(Y)))
        Z_ = np.zeros((len(X), len(Y)))

        for y in range(len(Y)):
            val, vec, Q = ops.eig_to_ham(measurements, len(operators), Y[y], relative=False)
            for x in range(len(X)):
                Z[x, y] = np.linalg.norm(ops.perp_ham_subspace(h0, val, vec, n=X[x]))
                Z_[x, y] = val[x]

        X, Y = np.meshgrid(X, Y)

        fig, ax = plt.subplots(2, 1)

        mesh = ax[0].pcolormesh(X, Y, Z.T, vmin=0, vmax=0.16)
        ax[0].set_xlabel('number of eigenvalues in nullspace')
        ax[0].set_ylabel('stdev of uncorrelated noise')
        fig.colorbar(mesh, ax=ax[0]).set_label('norm of rejection of H0 onto nullspace')

        mesh = ax[1].pcolormesh(X, Y, Z_.T, vmin=-0.003)
        ax[1].set_xlabel('eigenvector number')
        ax[1].set_ylabel('stdev of uncorrelated noise')
        fig.colorbar(mesh, ax=ax[1]).set_label('eigenvector value')

        plt.show()

    else:
        print_(cfg, 'diagonalizing covariance...'); t_start = time.time()
        val, vec, Q = ops.eig_to_ham(measurements, len(operators))
        print_(cfg, 'that took {0:.1f} s'.format(time.time() - t_start))

        # exact hamiltonian
        data['h0'] = h0
        data['Q'] = Q
        data['eigens'] = []

        for x in range(min(print_eigens, len(operators))):
            data_entry = {}
            data_entry['variance'] = val[x]

            print_(cfg, '-- ΔH{0}:'.format(x), val[x])
            ev = vec[:, x]
            ev *= np.sign(ev[0])

            data_entry['h'] = ev

            # print terms in hamiltonian
            if x <= 2:
                print_(cfg, '---- H{0}:'.format(x))

                for i in range(ev.shape[0]):
                    # if abs(h0[i] - ev[i]) > 1e-5:
                        print_(cfg, '{2} * {0} (D = {1})'.format(operators[i].name, h0[i] - ev[i], ev[i]))

                def aniso(i):
                    return (ev[3 * i + 2] - ev[3 * i]) / np.sqrt(4 * ev[3 * i] ** 2 + ev[3 * i + 2] ** 2)

                data_entry['anisotropy'] = []
                if cfg['ops_dist'] == 3 and 'rot' in cfg['ops_symmetries'].split(',') and 'su2' in cfg['ops_symmetries'].split(','):
                    if len(ev) == 2:
                        data_entry['j3'] = ev[1] / ev[0]
                        data_entry['simple_h'] = 'H ~ {0:.5f}*(j1j2) + {1:.5f}*(3n)'.format(ev[0], ev[1])
                    else:
                        data_entry['j2'] = ev[1] / ev[0]
                        data_entry['j3'] = ev[2] / ev[0]
                        data_entry['simple_h'] = 'H ~ {0:.5f}*(1n) + {1:.5f}*(2n) + {2:.5f}*(3n)'.format(ev[0], ev[1], ev[2])
                elif cfg['ops_dist'] == 4 and 'rot' in cfg['ops_symmetries'].split(','):
                    if 'su2' in cfg['ops_symmetries'].split(','):
                        data_entry['j2'] = ev[1] / ev[0]
                        data_entry['j3'] = ev[2] / ev[0]
                        data_entry['j4'] = ev[3] / ev[0]
                        data_entry['simple_h'] = 'H ~ {0:.5f}*(1n) + {1:.5f}*(2n) + {2:.5f}*(3n) + {3:.5f}*(4n)'.format(ev[0], ev[1], ev[2], ev[3])
                    else:
                        for i in range(4):
                            data_entry['anisotropy'].append({ 'name': '{0}n'.format(i + 1), 'value': aniso(i) })

                        data_entry['j2'] = sum(ev[3:6]) / sum(ev[0:3])
                        data_entry['simple_h'] = 'H ~ {0:.5f}*(1n) + {1:.5f}*(2n) + {2:.5f}*(3n) + {3:.5f}*(4n)'.format(np.mean(ev[0:3]), np.mean(ev[3:6]), np.mean(ev[6:9]), np.mean(ev[9:12]))
                elif cfg['ops_dist'] == 2 and 'rot' not in cfg['ops_symmetries'].split(','):
                    data_entry['anisotropy'].append({ 'name': '(01)', 'value': aniso(0) })
                    data_entry['anisotropy'].append({ 'name': '(10)', 'value': aniso(1) })
                    data_entry['anisotropy'].append({ 'name': '(1-1)', 'value': aniso(2) })
                    data_entry['anisotropy'].append({ 'name': '(11)', 'value': aniso(3) })
                    # print_('rotation sb (10)-(01): {0:.3f}%'.format(100 * (ev[0] - ev[3]) / ev[0]))
                    # print_('rotation sb (11)-(1-1): {0:.3f}%'.format(100 * (ev[6] - ev[9]) / ev[6]))
                    # print_('xy term order: {0:.3f}%'.format(100 * np.mean(ev[12::2])))

                    data_entry['j2'] = sum(ev[6:12]) / sum(ev[0:6])
                    data_entry['simple_h'] = 'H ~ {0:.5f}*(01) + {1:.5f}*(10) + {2:.5f}*(11) + {3:.5f}*(1-1)'.format(np.mean(ev[0:3]), np.mean(ev[3:6]), np.mean(ev[6:9]), np.mean(ev[9:12]))

            data['eigens'].append(data_entry)

        variance_exact = ops.braket_variance(Q, h0)
        data['variance'] = variance_exact
        variance_best = val[0]

        print_(cfg, 'variance of exact hamiltonian with Q:', variance_exact)
        print_(cfg, 'variance of best hamiltonian with Q:', variance_best)
        if 'Energy' in measurements and 'Sigma' in measurements['Energy']:
            print_(cfg, 'variance of exact hamiltonian with MC:', measurements['Energy']['Sigma'])

        data['rejection'] = []

        for i in range(len(val)):
            data['rejection'].append(np.linalg.norm(ops.perp_ham_subspace(h0, val, vec, n=i)))

        if plot_rejection:
            Y = []
            for i in range(50):
                Y.append(np.linalg.norm(ops.perp_ham_subspace(h0, val, vec, n=i)))

            plt.scatter(np.arange(0, 50), Y)
            plt.yscale('log')
            # plt.xticks(ticks=np.arange(0, 50), labels=[str(i) for i in val[:50]], rotation=-90)
            plt.show()

        else:
            rej = ops.perp_ham_subspace(h0, val, vec, subspace_eps)
            print_(cfg, 'rejection of h0 onto subspace of best eigenvectors has norm', np.linalg.norm(rej))
            print_(cfg, 'projection of h0 onto subspace of best eigenvectors has norm', np.linalg.norm(h0- rej))

    if print_correlators:
        for index, op in enumerate(operators):
            print_(cfg, op.name, measurements[str(index)]['Mean'])

    return data


def main(argv):
    found_config = False
    run_data = None

    # load a configuration file name if first argument specified as @conf_file
    if argv[1][0] == '@':
        # load relative path if @@dir/conf_file.json
        if argv[1][1] == '@':
            cfg, run_data = load_run(argv[1][2:], True)
        else:
            cfg, run_data = load_run(argv[1][1:])
        found_config = True
    else: 
        cfg = config.default()
        cfg['geom_len_x'] = int(argv[1].split('x')[0])
        cfg['geom_len_y'] = int(argv[1].split('x')[1])
        found_config = False


    # command line options - not part of configuration
    plot_correlators = False
    plot_perturb_correlators = False
    plot_training_energy = False
    force_retrain = False
    force_reexact = False
    force_resample = False
    skip_meas = False
    exact_mode = False
    find_machine = False
    diag_only = False
    print_eigens = 8
    subspace_eps = 1e-7
    plot_rejection = False


    # read command line options, including config parameters
    argc = 2
    while argc < len(argv):
        arg = argv[argc]

        if arg == '-plot_training_energy':
            plot_training_energy = True
        elif arg == '-retrain':
            force_retrain = True
        elif arg == '-reexact':
            force_reexact = True
        elif arg == '-resample':
            force_resample = True
        elif arg == '-plot_correlators':
            plot_correlators = True
        elif arg == '-exact':
            exact_mode = True
        elif arg == '-skip_meas':
            skip_meas = True
        elif arg == '-find_machine':
            find_machine = True
        elif arg == '-diag_only':
            diag_only = True
        elif arg == '-plot_rejection':
            plot_rejection = True
        elif arg == '-plot_perturb_correlators':
            plot_perturb_correlators = True
        elif arg == '-print_eigens':
            argc += 1
            print_eigens = int(argv[argc])
        elif arg == '-subspace_eps':
            argc += 1
            subspace_eps = float(argv[argc])
        elif not found_config and arg[1:] in config.all_options():
            # config options
            argc += 1
            cfg[arg[1:]] = config.parse_option(arg[1:], argv[argc])
        else:
            print_(cfg, 'unrecognized argument:', arg)
            sys.exit(1)

        argc += 1

    
    # if config wasn't specified explicitly, do verification
    if not found_config:
        if cfg['mpi_threads'] != comm.Get_size():
            print_(cfg, 'argument mpi_threads does not match number of MPI processes')
            sys.exit(1)

        config.fix(cfg)

    # no multiprocessing for exact solving
    if exact_mode and not master_thread():
        sys.exit(0)
    if exact_mode:
        if cfg['exact_eigens'] is None:
            print_(cfg, 'please set exact_eigens to a nonzero value')
            sys.exit(0)

    
    if diag_only:
        path = None
    else:
        run_name = config.hash(cfg)
        if master_thread():
            print_(cfg, 'This configuration is called', run_name)

        if cfg['output_file'] is not None:
            path = cfg['output_file']
        else:
            path = 'runs/{0}.json'.format(run_name)

    if not os.path.exists('runs'):
        os.mkdir('runs')

    ### prepare the run
    nk.utils.seed(cfg['sampler_seed'])
    operators, lat, graph, hi, ham = prepare_hilbert(cfg)

    if master_thread():
        print_(cfg, 'Configuration: ', cfg)
        print_(cfg, '{0} operators with {1} terms total'.format(len(operators), sum(len(op.terms) for op in operators)))
        if not diag_only:
            run_data = prepare_run_data(cfg, path, run_name)

        run_tasks = prepare_run_tasks(cfg, run_data, force_retrain, force_resample, force_reexact, find_machine, exact_mode, diag_only, skip_meas)
    else:
        run_tasks = None
    run_tasks = comm.bcast(run_tasks, root=0)

    ### train the model
    ffnn, sa, opt, __keep_alive = prepare_nn(cfg, lat, hi, graph)

    if run_tasks['train'] == 'do':
        if master_thread(): print_(cfg, 'converging gs...'); t_start = time.time()
        train_model(cfg, run_data, ham, sa, opt, ffnn, plot_training_energy, hi)

        if master_thread():
            print_(cfg, 'that took {0:.1f} s'.format(time.time() - t_start))
            save_run_data(run_data, path)

    elif run_tasks['train'] == 'load':
        print_(cfg, 'loading training wf')
        load_model(cfg, run_data, ffnn)
    elif run_tasks['train'] == 'skip':
        print_(cfg, 'skipping training wf')
        pass


    ### ED
    exact_wf = None
    if master_thread():
        if run_tasks['exact'] == 'do':
            print_(cfg, 'calculating exact gs...'); t_start = time.time()
            exact_wf = calculate_exact_gs(cfg, hi, run_data, ham)
            print_(cfg, 'that took {0:.1f} s'.format(time.time() - t_start))

            save_run_data(run_data, path)
        elif run_tasks['exact'] == 'skip':
            print_(cfg, 'skipping finding exact wf')

            if 'exact' in run_data:
                exact_wf = {
                        'vals': run_data['exact']['vals'],
                        'vecs': [np.array([complex(x) for x in vec]) for vec in run_data['exact']['vecs']]
                }

    ### report gs info
    if master_thread():
        if 'wf_energy' in run_data:
            if 'wf_energy_err' in run_data:
                print_(cfg, 'nn gs energy:', run_data['wf_energy'], '+-', run_data['wf_energy_err'])
            else:
                print_(cfg, 'nn gs energy:', run_data['wf_energy'])

        if exact_wf is not None:
            if 'wf_energy' in run_data:
                print_(cfg, 'exact gs energy:', exact_wf['vals'][0], '(Δ={0:.4f})'.format(run_data['wf_energy'] - exact_wf['vals'][0]))
            else:
                print_(cfg, 'exact gs energy:', exact_wf['vals'][0])

    ### measurements on nn

    # get nk observables
    if master_thread():
        t_start = time.time()
        print_(cfg, 'converting observables...')

    rotate_sites = lat.rotated_sites(cfg['ham_rotation'])

    if run_tasks['meas'] == 'do' or run_tasks['exact_meas'] == 'do':
        if cfg['threading_total'] is not None:
            observables = nkutil.convert_observables(hi, operators, rotate_sites, cfg['threading_index'], cfg['threading_total'])
        else:
            observables = nkutil.convert_observables(hi, operators, rotate_sites)

    if master_thread():
        print_(cfg, 'that took {0:.1f} s'.format(time.time() - t_start))


    if run_tasks['meas'] == 'do':
        if cfg['meas_full_sample']:
            if master_thread():
                print_(cfg, 'measuring observables (full sampling)...'); t_start = time.time()

                measure_full_sampling(cfg, run_data, hi, ffnn, operators, observables, ham, lat)
                print_(cfg, 'that took {0:.1f} s'.format(time.time() - t_start))
        else:
            print_(cfg, 'measuring {0} observables using MC...'.format(len(observables)))
            measure_mc2(cfg, run_data, ham, ffnn, sa, observables, path)
            # measure_mc(cfg, run_data, observables, sa)

            for op_dict in run_data['meas'].values():
                op_dict['Mean'] = sum(x for x in op_dict['batch_means']) / len(op_dict['batch_means'])
                op_dict['Sigma'] = np.sqrt(sum(x * x for x in op_dict['batch_errors']) / len(op_dict['batch_errors']))

        if master_thread():
            save_run_data(run_data, path)

    elif run_tasks['meas'] == 'skip':
        if master_thread():
            print_(cfg, 'skipping MC measurements')

    ### exact measurements
    if master_thread():
        if run_tasks['exact_meas'] == 'do':
            print_(cfg, 'measuring observables using exact wf...'); t_start = time.time()
            measure_exact_wf(cfg, run_data, exact_wf, observables)
            print_(cfg, 'that took {0:.1f} s'.format(time.time() - t_start)) 

            save_run_data(run_data, path)
        elif run_tasks['exact_meas'] == 'skip':
            print_(cfg, 'skipping exact measurements')

    if not master_thread():
        sys.exit(0)

    if run_tasks['diag'] == 'do':
        return diagonalize(cfg, hi, ham, ffnn, lat, rotate_sites, run_data, run_data['exact_meas' if exact_mode else 'meas'], operators, print_eigens, subspace_eps, plot_rejection, plot_perturb_correlators, plot_correlators)
    else:
        print_(cfg, 'skipping diag')

if __name__ == "__main__":
    """
    data = main(['python3 e2h.py', '4x4', '-ops_dist', '2', '-ham_j2', '1.0', '-exact', '-meas_full_sample_sym_c4', '1', '-meas_full_sample_sym_t', '1', '-ops_symmetries', 'trans_x,trans_y,rot,su2', '-exact_eigens', '1'])
    data = main(['python3 e2h.py', '4x4', '-ops_dist', '3', '-ham_j2', '1.0', '-exact', '-meas_full_sample_sym_c4', '1', '-meas_full_sample_sym_t', '1', '-ops_symmetries', 'trans_x,trans_y,rot,su2', '-exact_eigens', '1'])
    data = main(['python3 e2h.py', '4x4', '-ops_dist', '2', '-ham_j2', '1.0', '-exact', '-meas_full_sample_sym_c4', '1', '-meas_full_sample_sym_t', '1', '-ops_symmetries', 'trans_x,trans_y,rot,su2', '-exact_eigens', '1', '-ops_only_aniso', '2'])
    """


    if True: # rbm
        ham_j2 = ['0.0', '0.1', '0.2', '0.3', '0.4', '0.45', '0.5', '0.55', '0.6', '0.75', '1.0', '1.25', '1.5', '1.75']
        j2_file = ham_j2

        cnn_file = "rbm/rbm_alpha_10_j1_1_j2_{0}.wf"
        out_dir = "rbm_test_data/"
        def write_data(cnn_wf, f, data):
            recons_h = nkutil.reconstructed_ham(data['hi'], data['rotate_sites'], data['operators'], data['eigens'][0]['h'])
            recons_gs = nk.exact.lanczos_ed(recons_h, first_n=2, compute_eigenvectors=True)

            cnn_recons_energy = np.real(np.vdot(cnn_wf, recons_h.to_sparse() * cnn_wf))
            cnn_recons_variance = data['eigens'][0]['variance']
            original_variance = data['variance']
            recons_gs_energy = recons_gs.eigenvalues[0]
            cnn_recons_fidelity = np.abs(np.vdot(cnn_wf, recons_gs.eigenvectors[0])) ** 2
            exact_recons_fidelity = np.abs(np.vdot(exact_gs.eigenvectors[0], recons_gs.eigenvectors[0])) ** 2
            components = ','.join(str(x) for x in data['eigens'][0]['h'])
            rejection = data['rejection'][0]

            f.write('{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}\n'.format(j2, cnn_recons_energy, cnn_recons_variance, original_variance, recons_gs_energy, cnn_recons_fidelity, exact_recons_fidelity, rejection, components, data['eigens'][1]['variance'], recons_gs.eigenvalues[1] - recons_gs.eigenvalues[0]))
            f.flush()

        """
        for j2 in ham_j2:
            data = main(['python3 e2h.py', '4x4', '-exact', '-exact_eigens', '1', '-ops_dist', '2', '-ham_j2', j2, '-meas_full_sample', '1', '-meas_full_sample_sym_c4', '1', '-meas_full_sample_sym_t', '1', '-ops_symmetries', 'trans_x,trans_y,rot', '-ops_only_aniso', '5'])
        """

        """
        with open(out_dir + 'j1_j2_j1j2zz_j3.csv', 'w') as j1_j2_j1j2zz_j3:
            for j2 in ham_j2:
                data = main(['python3 e2h.py', '4x4', '-machine_type', 'rbms', '-machine_rbms_alpha', '10', '-ops_dist', '2', '-ham_j2', j2, '-machine_from_file', cnn_file.format(j2), '-meas_full_sample', '1', '-meas_full_sample_sym_c4', '1', '-meas_full_sample_sym_t', '1', '-ops_symmetries', 'trans_x,trans_y,rot', '-ops_only_aniso', '5'])
                cnn_wf = extract_wavefunction(data['cfg'], data['hi'], data['ham'], data['ffnn'], data['lat'], character=(1, -1) if float(j2) > 2 else (1, 1))

                if float(j2) > 1: 
                    proj = np.array([0., 0., 0., 0.])
                    h0 = np.array([1/(1+float(j2)), float(j2)/(1+float(j2)), 0, 0])
                    proj += np.dot(h0, data['eigens'][0]['h']) * data['eigens'][0]['h']
                    proj += np.dot(h0, data['eigens'][1]['h']) * data['eigens'][1]['h']

                    recons_h = nkutil.reconstructed_ham(data['hi'], data['rotate_sites'], data['operators'], proj)
                    recons_gs = nk.exact.lanczos_ed(recons_h, first_n=2, compute_eigenvectors=True)
                    cnn_recons_energy = np.real(np.vdot(cnn_wf, recons_h.to_sparse() * cnn_wf))

                    j1_j2_j1j2zz_j3.write('{0}'.format(','.join(str(x) for x in proj)))
                else:
                    recons_h = nkutil.reconstructed_ham(data['hi'], data['rotate_sites'], data['operators'], data['eigens'][0]['h'])
                    recons_gs = nk.exact.lanczos_ed(recons_h, first_n=2, compute_eigenvectors=True)
                    cnn_recons_energy = np.real(np.vdot(cnn_wf, recons_h.to_sparse() * cnn_wf))

                    j1_j2_j1j2zz_j3.write('{0}'.format(','.join(str(x) for x in data['eigens'][0]['h'])))
                j1_j2_j1j2zz_j3.write(f",{data['variance']},{data['eigens'][0]['variance']},{data['eigens'][1]['variance']},{recons_gs.eigenvalues[1] - recons_gs.eigenvalues[0]},{recons_gs.eigenvalues[0]},{cnn_recons_energy}\n")
                """


        """
        with open(out_dir + 'common.csv', 'w') as common:
            with open(out_dir + 'j1_j2.csv', 'w') as j1_j2:
                with open(out_dir + 'j1j2_j3.csv', 'w') as j1j2_j3:
                    with open(out_dir + 'j1j2_j1j2zz.csv', 'w') as j1j2_j1j2zz:
                        with open(out_dir + 'j1_j2_j1j2zz.csv', 'w') as j1_j2_j1j2zz:
                            for j2 in ham_j2:
                                data = main(['python3 e2h.py', '4x4', '-machine_type', 'rbms', '-machine_rbms_alpha', '10', '-ops_dist', '2', '-ham_j2', j2, '-machine_from_file', cnn_file.format(j2), '-meas_full_sample', '1', '-meas_full_sample_sym_c4', '1', '-meas_full_sample_sym_t', '1', '-ops_symmetries', 'trans_x,trans_y,rot,su2'])
                                aniso = ops.symmetrized_operators(data['lat'], n_point=2, dist=3, symmetries='trans_x,trans_y,rot')
                                aniso = [op for op in aniso if 'one_site' not in op.tags]
                                aniso0 = nkutil.convert_operator(data['hi'], aniso[0].terms, aniso[0].coefs, data['rotate_sites'])
                                aniso1 = nkutil.convert_operator(data['hi'], aniso[2].terms, aniso[2].coefs, data['rotate_sites'])
                                aniso2 = nkutil.convert_operator(data['hi'], aniso[3].terms, aniso[3].coefs, data['rotate_sites'])
                                aniso3 = nkutil.convert_operator(data['hi'], aniso[5].terms, aniso[5].coefs, data['rotate_sites'])
                                aniso4 = nkutil.convert_operator(data['hi'], aniso[6].terms, aniso[6].coefs, data['rotate_sites'])
                                aniso5 = nkutil.convert_operator(data['hi'], aniso[8].terms, aniso[8].coefs, data['rotate_sites'])

                                terms = []
                                coefs = []
                                for i in range(4):
                                    for j in range(4):
                                        for s in range(1, 4):
                                            terms.append([(0, s, 0), (4*j+i, s, 0)])
                                            coefs.append(np.cos(i*np.pi+j*np.pi)+1j*np.sin(i*np.pi+j*np.pi))
                                pipi = nkutil.convert_operator(data['hi'], terms, coefs, data['rotate_sites'])

                                terms = []
                                coefs = []
                                for i in range(4):
                                    for j in range(4):
                                        for s in range(1, 4):
                                            terms.append([(0, s, 0), (4*j+i, s, 0)])
                                            coefs.append(np.cos(i*np.pi)+1j*np.sin(i*np.pi))
                                pizero = nkutil.convert_operator(data['hi'], terms, coefs, data['rotate_sites'])

                                exact_gs = nk.exact.lanczos_ed(data['ham'], first_n=1, compute_eigenvectors=True)
                                cnn_wf = extract_wavefunction(data['cfg'], data['hi'], data['ham'], data['ffnn'], data['lat'], character=(1, -1) if float(j2) > 2 else (1, 1))
                                cnn_energy_exact = data['energy']
                                cnn_variance_exact = data['variance']

                                common.write('{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}\n'.format(j2, exact_gs.eigenvalues[0], cnn_energy_exact, cnn_variance_exact, np.abs(np.vdot(cnn_wf, exact_gs.eigenvectors[0]))**2,
                                    np.real(np.vdot(cnn_wf, aniso0.to_sparse() * cnn_wf)),
                                    np.real(np.vdot(cnn_wf, aniso1.to_sparse() * cnn_wf)),
                                    np.real(np.vdot(cnn_wf, aniso2.to_sparse() * cnn_wf)),
                                    np.real(np.vdot(cnn_wf, aniso3.to_sparse() * cnn_wf)),
                                    np.real(np.vdot(cnn_wf, aniso4.to_sparse() * cnn_wf)),
                                    np.real(np.vdot(cnn_wf, aniso5.to_sparse() * cnn_wf)),
                                    np.real(np.vdot(cnn_wf, pipi.to_sparse() * cnn_wf)),
                                    np.real(np.vdot(cnn_wf, pizero.to_sparse() * cnn_wf))))


                                # J1 J2
                                write_data(cnn_wf, j1_j2, data)

                                # J1 J2 J3
                                data = main(['python3 e2h.py', '4x4', '-machine_type', 'rbms', '-machine_rbms_alpha', '10', '-ops_dist', '3', '-ham_j2', j2, '-machine_from_file', cnn_file.format(j2), '-meas_full_sample', '1', '-meas_full_sample_sym_c4', '1', '-meas_full_sample_sym_t', '1', '-ops_symmetries', 'trans_x,trans_y,rot,su2'])
                                write_data(cnn_wf, j1j2_j3, data)

                                # J1J2 J1J2zz
                                data = main(['python3 e2h.py', '4x4', '-machine_type', 'rbms', '-machine_rbms_alpha', '10', '-ops_dist', '2', '-ham_j2', j2, '-machine_from_file', cnn_file.format(j2), '-meas_full_sample', '1', '-meas_full_sample_sym_c4', '1', '-meas_full_sample_sym_t', '1', '-ops_symmetries', 'trans_x,trans_y,rot', '-ops_only_aniso', '2'])
                                write_data(cnn_wf, j1j2_j1j2zz, data)
                                """

    if True: # cnn
        ham_j2 = ['0.0', '0.1', '0.2', '0.3', '0.4', '0.45', '0.5', '0.55', '0.6', '0.65', '0.75', '0.85', '1.0', '1.25', '1.5', '1.75', '2.0']

        cnn_file = "cnn_exact/j1_1_j2_{0}.wf"
        out_dir = "cnn_test_data/"


        def write_data(cnn_wf, f, data):
            recons_h = nkutil.reconstructed_ham(data['hi'], data['rotate_sites'], data['operators'], data['eigens'][0]['h'])
            recons_gs = nk.exact.lanczos_ed(recons_h, first_n=2, compute_eigenvectors=True)
            print('gap', recons_gs.eigenvalues)

            cnn_recons_variance = data['eigens'][0]['variance']
            original_variance = data['variance']
            cnn_recons_energy = np.real(np.vdot(cnn_wf, recons_h.to_sparse() * cnn_wf))
            recons_gs_energy = recons_gs.eigenvalues[0]
            cnn_recons_fidelity = np.abs(np.vdot(cnn_wf, recons_gs.eigenvectors[0])) ** 2
            exact_recons_fidelity = np.abs(np.vdot(exact_gs.eigenvectors[0], recons_gs.eigenvectors[0])) ** 2
            components = ','.join(str(x) for x in data['eigens'][0]['h'])
            rejection = data['rejection'][0]

            f.write('{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}\n'.format(j2, cnn_recons_energy, cnn_recons_variance, original_variance, recons_gs_energy, cnn_recons_fidelity, exact_recons_fidelity, rejection, components, data['eigens'][1]['variance'], recons_gs.eigenvalues[1] - recons_gs.eigenvalues[0]))
            f.flush()

        with open(out_dir + 'j1_j2_j1j2zz_j3.csv', 'w') as j1_j2_j1j2zz_j3:
            for j2 in ham_j2:
                data = main(['python3 e2h.py', '4x4', '-machine_type', 'j1j2_exact', '-ops_dist', '2', '-ham_j2', j2, '-machine_from_file', cnn_file.format(j2), '-meas_full_sample', '1', '-meas_full_sample_sym_c4', '1', '-meas_full_sample_sym_t', '1', '-ops_symmetries', 'trans_x,trans_y,rot', '-meas_wf_rotation', 'neel' if float(j2) <= 0.5 else 'striped', '-ops_only_aniso', '5'])
                cnn_wf = extract_wavefunction(data['cfg'], data['hi'], data['ham'], data['ffnn'], data['lat'], character=(1, -1) if float(j2) > 2 else (1, 1))

                proj = np.array([0., 0., 0., 0.])
                proj += np.dot(data['h0'], data['eigens'][0]['h']) * data['eigens'][0]['h']
                proj += np.dot(data['h0'], data['eigens'][1]['h']) * data['eigens'][1]['h']

                proj = proj / proj[0]
                print("proj:", proj)
                print("variance of projected hamiltonian", ops.braket_variance(data['Q'], proj))


                if float(j2) > 1:
                    recons_h = nkutil.reconstructed_ham(data['hi'], data['rotate_sites'], data['operators'], proj)
                    recons_gs = nk.exact.lanczos_ed(recons_h, first_n=2, compute_eigenvectors=True)
                    cnn_recons_energy = np.real(np.vdot(cnn_wf, recons_h.to_sparse() * cnn_wf))

                    j1_j2_j1j2zz_j3.write('{0}'.format(','.join(str(x) for x in proj)))
                else:
                    recons_h = nkutil.reconstructed_ham(data['hi'], data['rotate_sites'], data['operators'], data['eigens'][0]['h'])
                    recons_gs = nk.exact.lanczos_ed(recons_h, first_n=2, compute_eigenvectors=True)
                    cnn_recons_energy = np.real(np.vdot(cnn_wf, recons_h.to_sparse() * cnn_wf))

                    j1_j2_j1j2zz_j3.write('{0}'.format(','.join(str(x) for x in data['eigens'][0]['h'])))
                j1_j2_j1j2zz_j3.write(f",{data['variance']},{data['eigens'][0]['variance']},{data['eigens'][1]['variance']},{recons_gs.eigenvalues[1] - recons_gs.eigenvalues[0]},{recons_gs.eigenvalues[0]},{cnn_recons_energy}\n")


        """
        with open(out_dir + 'common.csv', 'w') as common:
            with open(out_dir + 'j1_j2.csv', 'w') as j1_j2:
                with open(out_dir + 'j1j2_j3.csv', 'w') as j1j2_j3:
                    with open(out_dir + 'j1j2_j1j2zz.csv', 'w') as j1j2_j1j2zz:
                        with open(out_dir + 'j1_j2_j1j2zz.csv', 'w') as j1_j2_j1j2zz:
                            for j2 in ham_j2:
                                data = main(['python3 e2h.py', '4x4', '-machine_type', 'j1j2_exact', '-ops_dist', '2', '-ham_j2', j2, '-machine_from_file', cnn_file.format(j2), '-meas_full_sample', '1', '-meas_full_sample_sym_c4', '1', '-meas_full_sample_sym_t', '1', '-ops_symmetries', 'trans_x,trans_y,rot,su2', '-meas_wf_rotation', 'neel' if float(j2) <= 0.5 else 'striped'])
                                aniso = ops.symmetrized_operators(data['lat'], n_point=2, dist=3, symmetries='trans_x,trans_y,rot')
                                aniso = [op for op in aniso if 'one_site' not in op.tags]
                                aniso0 = nkutil.convert_operator(data['hi'], aniso[0].terms, aniso[0].coefs, data['rotate_sites'])
                                aniso1 = nkutil.convert_operator(data['hi'], aniso[2].terms, aniso[2].coefs, data['rotate_sites'])
                                aniso2 = nkutil.convert_operator(data['hi'], aniso[3].terms, aniso[3].coefs, data['rotate_sites'])
                                aniso3 = nkutil.convert_operator(data['hi'], aniso[5].terms, aniso[5].coefs, data['rotate_sites'])
                                aniso4 = nkutil.convert_operator(data['hi'], aniso[6].terms, aniso[6].coefs, data['rotate_sites'])
                                aniso5 = nkutil.convert_operator(data['hi'], aniso[8].terms, aniso[8].coefs, data['rotate_sites'])
                                terms = []
                                coefs = []
                                for i in range(4):
                                    for j in range(4):
                                        for s in range(1, 4):
                                            terms.append([(0, s, 0), (4*j+i, s, 0)])
                                            coefs.append(np.cos(i*np.pi+j*np.pi)+1j*np.sin(i*np.pi+j*np.pi))
                                pipi = nkutil.convert_operator(data['hi'], terms, coefs, data['rotate_sites'])

                                terms = []
                                coefs = []
                                for i in range(4):
                                    for j in range(4):
                                        for s in range(1, 4):
                                            terms.append([(0, s, 0), (4*j+i, s, 0)])
                                            coefs.append(np.cos(i*np.pi)+1j*np.sin(i*np.pi))
                                pizero = nkutil.convert_operator(data['hi'], terms, coefs, data['rotate_sites'])



                                exact_gs = nk.exact.lanczos_ed(data['ham'], first_n=1, compute_eigenvectors=True)
                                cnn_wf = extract_wavefunction(data['cfg'], data['hi'], data['ham'], data['ffnn'], data['lat'], character=(1, -1) if float(j2) > 2 else (1, 1))
                                cnn_energy_exact = data['energy']
                                cnn_variance_exact = data['variance']

                                common.write('{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}\n'.format(j2, exact_gs.eigenvalues[0], cnn_energy_exact, cnn_variance_exact, np.abs(np.vdot(cnn_wf, exact_gs.eigenvectors[0]))**2,
                                    np.real(np.vdot(cnn_wf, aniso0.to_sparse() * cnn_wf)),
                                    np.real(np.vdot(cnn_wf, aniso1.to_sparse() * cnn_wf)),
                                    np.real(np.vdot(cnn_wf, aniso2.to_sparse() * cnn_wf)),
                                    np.real(np.vdot(cnn_wf, aniso3.to_sparse() * cnn_wf)),
                                    np.real(np.vdot(cnn_wf, aniso4.to_sparse() * cnn_wf)),
                                    np.real(np.vdot(cnn_wf, aniso5.to_sparse() * cnn_wf)),
                                    np.real(np.vdot(cnn_wf, pipi.to_sparse() * cnn_wf)),
                                    np.real(np.vdot(cnn_wf, pizero.to_sparse() * cnn_wf))))

                                # J1 J2
                                write_data(cnn_wf, j1_j2, data)

                                # J1J2 J3
                                data = main(['python3 e2h.py', '4x4', '-machine_type', 'j1j2_exact', '-ops_dist', '3', '-ham_j2', j2, '-machine_from_file', cnn_file.format(j2), '-meas_full_sample', '1', '-meas_full_sample_sym_c4', '1', '-meas_full_sample_sym_t', '1', '-ops_symmetries', 'trans_x,trans_y,rot,su2', '-meas_wf_rotation', 'neel' if float(j2) <= 0.5 else 'striped'])
                                write_data(cnn_wf, j1j2_j3, data)

                                # J1J2 J1J2zz
                                data = main(['python3 e2h.py', '4x4', '-machine_type', 'j1j2_exact', '-ops_dist', '2', '-ham_j2', j2, '-machine_from_file', cnn_file.format(j2), '-meas_full_sample', '1', '-meas_full_sample_sym_c4', '1', '-meas_full_sample_sym_t', '1', '-ops_symmetries', 'trans_x,trans_y,rot', '-meas_wf_rotation', 'neel' if float(j2) <= 0.5 else 'striped', '-ops_only_aniso', '2'])
                                write_data(cnn_wf, j1j2_j1j2zz, data)
                                """



    main(sys.argv)
