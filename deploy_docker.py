import subprocess
import glob
import os
import sys
import json

def list_servers():
    lines = subprocess.run(['openstack', 'server', 'list'], stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')
    divs = [i for i, char in enumerate(lines[1]) if char == '|']

    if len(divs) != 7:
        raise RuntimeError('unexpected')

    for line in lines[3:-2]:
        cores = int(line[divs[5]+1:divs[6]-1].strip().split('.')[0][1:])
        name = line[divs[1]+1:divs[2]-1].strip()
        status = line[divs[2]+1:divs[3]-1].strip()
        ip = line[divs[3]+1:divs[4]-1].strip().split('=')[1]
        yield (name, status, ip, cores)

if sys.argv[1] == 'exec':
    cluster_servers = [x for x in list_servers() if x[1] == 'ACTIVE' and x[0].startswith('e2h')]
    n_cores = sum(x[3] for x in cluster_servers)
    cur_index = 0
    processes = []

    for server in cluster_servers:
        processes.append([])
        for i in range(server[3]):
            cmd = 'docker run -d skkestrel/netket '
            cmd += ' '.join(sys.argv[2:])
            cmd += ' -threading_index {0} -threading_total {1} -output_file out.json'.format(cur_index, n_cores)

            processes[-1].append(subprocess.Popen(['ssh', 'ubuntu@{0}'.format(server[2]), cmd],
                    shell=False,
                    stderr=subprocess.PIPE,
                    stdout=subprocess.PIPE))

            cur_index += 1

    with open('current', 'w') as f:
        for server, procs in zip(cluster_servers, processes):
            for proc in procs:
                proc.wait()
                f.write('{0} {1} {2}'.format(server[0], server[2], proc.stdout.readlines()[0].decode('utf-8')))
elif sys.argv[1] == 'stop':
    processes = []
    with open('current', 'r') as f:
        servers = []
        for line in f:
            servers.append((line.split()[0], line.split()[1]))

        cmd = 'docker stop $(docker ps -a -q)'
        if len(sys.argv) >= 3 and sys.argv[2] == 'clean':
            cmd += ' && docker rm $(docker ps -a -q)'


        servers = list(set(servers))
        for server in servers:
            processes.append((server,
                        subprocess.Popen(['ssh', 'ubuntu@{0}'.format(server[1]), cmd],
                        shell=False,
                        stderr=subprocess.PIPE,
                        stdout=subprocess.PIPE)))

    for server, proc in processes:
        proc.wait()
        print('***************************************')
        print('output from {0} {1}:'.format(server[0], server[1]))
        print('***************************************')
        for line in (x.decode('utf-8').rstrip('\n') for x in proc.stdout.readlines()):
            print(line)
elif sys.argv[1] == 'pull':
    processes = []
    number = 0
    try:
        os.mkdir('gather')
    except OSError:
        pass

    files = glob.glob('gather/*')
    for f in files:
        os.remove(f)

    with open('current', 'r') as f:
        for line in f:
            server = (line.split()[0], line.split()[1])
            container = line.split()[2]

            print('process started')
            processes.append((server,
                    subprocess.Popen(['ssh', 'ubuntu@{0}'.format(server[1]), 'docker cp {0}:/home/dev/src/out.json {1}.json'.format(container, number)],
                        shell=False,
                        stderr=subprocess.PIPE,
                        stdout=subprocess.PIPE)))

            number += 1

    for server, proc in processes:
        proc.wait()
        print('***************************************')
        print('output from {0} {1}:'.format(server[0], server[1]))
        print('***************************************')
        for line in (x.decode('utf-8').rstrip('\n') for x in proc.stdout.readlines()):
            print(line)
        for line in (x.decode('utf-8').rstrip('\n') for x in proc.stderr.readlines()):
            print(line)

    number = 0
    with open('current', 'r') as f:
        for line in f:
            server = (line.split()[0], line.split()[1])
            container = line.split()[2]

            subprocess.run(['scp', 'ubuntu@{0}:{1}.json'.format(server[1], number), 'gather'])
            number += 1

elif sys.argv[1] == 'combine':
    combined_data = { 'meas': {} }
    for run in glob.glob('gather/*.json'):
        with open(run, 'r') as f:
            try:
                data = json.load(f)
            except json.JSONDecodeError:
                print('*** could not decode file', run)
                continue

        for key in data['meas']:
            combined_data['meas'][key] = data['meas'][key]
            combined_data['cfg'] = data['cfg']

    with open('combined_data.json', 'w') as f:
        f.write(json.dumps(combined_data, indent=1))

else:
    cluster_servers = [x for x in list_servers() if x[1] == 'ACTIVE' and x[0].startswith('e2h')]
    processes = []

    for server in cluster_servers:
        cmd = ' '.join(sys.argv[1:])
        processes.append(subprocess.Popen(['ssh', 'ubuntu@{0}'.format(server[2]), cmd],
                shell=False,
                stderr=subprocess.PIPE,
                stdout=subprocess.PIPE))

    for server, proc in zip(cluster_servers, processes):
        proc.wait()
        print('***************************************')
        print('output from {0} {1}:'.format(server[0], server[2]))
        print('***************************************')
        for line in (x.decode('utf-8').rstrip('\n') for x in proc.stdout.readlines()):
            print(line)
