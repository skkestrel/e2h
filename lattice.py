from typing import Iterable, Tuple, List, Optional

class SquareLattice:
    def __init__(self, L: Tuple[int, int], periodic: Tuple[bool, bool]):
        self.len_x = L[0]
        self.len_y = L[1]

        self.periodic_x = periodic[0]
        self.periodic_y = periodic[1]


    def site_index(self, xy: Tuple[int, int]) -> int:
        """
        Get site index from
        """
        return xy[1] * self.len_x + xy[0] 


    def site_coords(self, index: int) -> Tuple[int, int]:
        return (index % self.len_x, index // self.len_x)


    def iter_site_indices(self) -> Iterable[int]:
        return range(self.len_x * self.len_y)


    def offset_site_index(self, index: int, offset: Tuple[int, int]) -> Optional[int]:
        coord = self.site_coords(index)
        coord = (coord[0] + offset[0], coord[1] + offset[1])
        if self.periodic_x:
            coord = (coord[0] % self.len_x, coord[1])
        if self.periodic_y:
            coord = (coord[0], coord[1] % self.len_y)

        if coord[0] < 0 or coord[0] >= self.len_x:
            return None
        if coord[1] < 0 or coord[1] >= self.len_y:
            return None

        return self.site_index(coord)


    def rotated_sites(self, rotation: str) -> List[int]:
        if rotation.lower() == 'none':
            return []

        if rotation.lower() == 'neel':
            rot = []
            for index in self.iter_site_indices():
                coords = self.site_coords(index)
                if (coords[0] % 2) != (coords[1] % 2):
                    rot.append(index)
            return rot

        if rotation.lower() == 'striped':
            rot = []
            for index in self.iter_site_indices():
                coords = self.site_coords(index)
                if (coords[1] % 2) == 0:
                    rot.append(index)
            return rot


    def symmetrized_group_indices(self, symmetries: List[str]) -> List[List[int]]:
        tx = "trans_x" in symmetries
        ty = "trans_y" in symmetries

        groups = []

        # no symmetries: return every site as an individual group
        if not tx and not ty:
            for i in self.iter_site_indices():
                groups.append([i])
        # all symmetries: all sites are in the same group
        elif tx and ty:
            groups = [[]]
            for i in self.iter_site_indices():
                groups[-1].append(i)
        else:
            # if tx: return rows
            # if ty: return columns
            groups = []
            for index in range(self.len_y if tx else self.len_x):
                groups.append([])
                for index2 in range(self.len_x if tx else self.len_y):
                    if tx:
                        groups[-1].append(self.site_index((index2, index)))
                    else:
                        groups[-1].append(self.site_index((index, index2)))
            
        return groups


    def unique_neighbours(self, dist):
        neighbours = []
        # take only from the first 45 degrees in the first quadrant, as all these points are guaranteed to be unique
        for x in range(dist + 1):
            for y in range(x + 1):
                if 0 < x * x + y * y:
                    neighbours.append((x, y))

        neighbours.sort(key=lambda k: k[0] ** 2 + k[1] ** 2)
        return neighbours[:dist]


    def symmetrized_group_offsets(self, offset: Tuple[int, int], remove_inversions: bool, symmetries: List[str]) -> List[List[Tuple[int, int]]]:
        """
        applies all point group symmetries to an offset, optionally removing points that are equivalent up to inversion
        """
        ix = "refl_x" in symmetries
        iy = "refl_y" in symmetries
        rot = "rot" in symmetries

        n = offset

        all_syms = set()
        # set of all rotated and inverted variations of the offset
        all_syms.update([n, (-n[1], n[0]), (-n[0], -n[1]), (n[1], -n[0])])

        # if point doesn't lie on one of the 8 cardinal directions, then add some inversions as well
        if not (n[0] == 0 or n[1] == 0 or n[0] == n[1]):
            all_syms.update([(n[1], n[0]), (-n[0], n[1]), (-n[1], -n[0]), (n[0], -n[1])])

        if remove_inversions:
            new_syms = set()

            for pt in all_syms:
                if (-pt[0], -pt[1]) not in new_syms:
                    new_syms.add(pt)

            all_syms = new_syms


        # fix issues with wrapping around borders of periodic systems
        new_syms = set()
        for pt in all_syms:
            # whether the same operator could potentially wrap around the other side to form the same operator
            # e.g. (2, 0) is the same operator as (-2, 0) in a 4x4 with pbc
            wrap_x = False
            wrap_y = False

            if self.periodic_x:
                if abs(pt[0] * 2) == self.len_x:
                    wrap_x = True
                elif abs(pt[0] * 2) > self.len_x:
                    # don't add points that are greater than half the width
                    continue

            if self.periodic_y:
                if abs(pt[1] * 2) == self.len_y:
                    wrap_y = True
                elif abs(pt[1] * 2) > self.len_y:
                    # don't add points that are greater than half the width
                    continue

            add = True

            if remove_inversions and (wrap_x or wrap_y):
                if (-pt[0], -pt[1]) in new_syms or \
                        (-pt[0], pt[1]) in new_syms or (pt[0], -pt[1]) in new_syms:
                    add = False

            if wrap_x:
                if wrap_y:
                    if (-pt[0], -pt[1]) in new_syms or \
                            (-pt[0], pt[1]) in new_syms or (pt[0], -pt[1]) in new_syms:
                        add = False
                else:
                    if (-pt[0], pt[1]) in new_syms:
                        add = False
            else:
                if wrap_y:
                    if (pt[0], -pt[1]) in new_syms:
                        add = False

            if add:
                new_syms.add(pt)

        all_syms = new_syms

        seen_points = set()
        groups = []

        if rot and (ix or iy):
            # all symmetries in the same group
            return [list(all_syms)]
        elif rot:
            # rotation only
            for i in all_syms:
                if i not in seen_points:
                    groups.append([])
                    rotations = [i, (-i[1], i[0]), (-i[0], -i[1]), (i[1], -i[0])]
                    for r in rotations:
                        # make sure that it wasn't removed by remove_inversions
                        if r in all_syms:
                            groups[-1].append(r)
                            seen_points.add(r)
        elif ix or iy:
            # inversion along some directions
            for i in all_syms:
                groups.append([])
                if i not in seen_points:
                    # enumerate inversion symmetries
                    if ix and iy:
                        inv = [i, (-i[0], i[1]), (i[0], -i[1]), (-i[0], -i[1])]
                    elif ix:
                        inv = [i, (-i[0], i[1])]
                    else:
                        inv = [i, (i[0], -i[1])]

                    for r in inv:
                        if r in all_syms:
                            groups[-1].append(r)
                            seen_points.add(r)
        else:
            # no symmetries
            for i in all_syms:
                groups.append([i])

        return groups


    def two_point_symmetrized_group_offsets(self, dist: int, symmetries: List[str], directional: bool) -> List[List[Tuple[int, int]]]:
        tx = "trans_x" in symmetries
        ty = "trans_y" in symmetries

        ix = "refl_x" in symmetries
        iy = "refl_y" in symmetries

        if (ix != iy or tx != ty) and "rot" in symmetries:
            raise ValueError("invalid rotational symmetry")

        unique_neighbours = self.unique_neighbours(dist)
        groups = []
        for n in unique_neighbours:
            for group in self.symmetrized_group_offsets(n, not directional, symmetries):
                groups.append(group)

        return groups
