from mpi4py import MPI
import netket as nk
import numpy as ap

import itertools
import sys
import os
import lattice
import ops
import json

run_path = sys.argv[1] if len(sys.argv) >= 2 else "wavefunctions/L6/j1j2_2d_L6_j2_2.0_striped"
run_name = os.path.basename(run_path)

tokens = run_name.split("_") 
if len(tokens) != 6:
    raise ValueError("unknown filetype (1)")

if tokens[0] != "j1j2":
    raise ValueError("unknown filetype (2)")

if tokens[1] != "2d":
    raise ValueError("unknown filetype (3)")

if tokens[2][0] != "L":
    raise ValueError("unknown filetype (4)")

if tokens[3] != "j2":
    raise ValueError("unknown filetype (5)")

L = int(tokens[2][1:])
J2 = float(tokens[4])
rotation = tokens[5].lower().capitalize()

print("L =", L, "J2 =", J2)

samples = 1000

lat = lattice.SquareLattice([L, L], [True, True])
operators = ops.symmetrized_operators(lat, 2, 2, ["trans_x", "trans_y", "rot"])

print("{0} operators".format(len(operators)))
for op in operators:
    print(op.name)

# simulate the model
data = json.loads(open("json/graph_L{0}.json".format(L)).read())
graph = nk.graph.CustomGraph(data["EdgeColors"], data["Automorphisms"])
hi = nk.hilbert.Spin(s=0.5, total_sz=0.0, graph=graph)
ham = ops.nk_j1j2(hi, J2)

# Define the machine
layers = (nk.layer.J1J2Convolutional(hilbert=hi,input_channels=1,output_channels=12),
          nk.layer.Lncosh(input_size=12*L*L),
          nk.layer.J1J2Convolutional(hilbert=hi,input_channels=12,output_channels=10),
          nk.layer.Relu(input_size=10*L*L),
          nk.layer.J1J2Convolutional(hilbert=hi,input_channels=10,output_channels=8),
          nk.layer.Relu(input_size=8*L*L),
          nk.layer.J1J2Convolutional(hilbert=hi,input_channels=8,output_channels=6),
          nk.layer.Relu(input_size=6*L*L),
          nk.layer.J1J2Convolutional(hilbert=hi,input_channels=6,output_channels=4),
          nk.layer.Relu(input_size=4*L*L),
          nk.layer.J1J2Convolutional(hilbert=hi,input_channels=4,output_channels=2),
          nk.layer.Relu(input_size=2*L*L),
          nk.layer.SumOutput(input_size=2*L*L))

# When the file is marked *_neel.wf you should put rottype="Neel"
# When the file is marked *_striped.wf you should put rottype="Striped"
# For L=6, for J2<=0.65 choose c4=0, for J2>0.65 choose c4=2
# For L=10, for J2<=0.55 choose c4=0, for J2>0.55 choose c4=2
c4 = 0
if L == 6:
    c4 = 0 if J2 < 0.65 else 2
if L == 10:
    c4 = 0 if J2 < 0.55 else 2

ffnn = nk.machine.J1J2Machine(hi, (), L, c4=c4, rottype=rotation)
ffnn.load(run_name + ".wf")

sa = nk.sampler.MetropolisExchange(machine=ffnn, graph=graph, d_max=2)
opt = nk.optimizer.Sgd(learning_rate=0)
gs = nk.variational.Vmc(
    hamiltonian=ham,
    sampler=sa,
    optimizer=opt,
    n_samples=samples,
    use_iterative=True,
    method="Gd",
)

print("adding observables")
ops.add_observables(hi, gs, operators)
print("measuring")
gs.run(output_prefix="test", n_iter=1)
print("finished")

val, vec, _ = ops.eig_to_ham(gs.get_observable_stats(), len(operators))

for x in range(3):
    print("eigenval:", val[x])
    ev = vec[:, x]
    print("eigenvec:")

    for i in range(ev.shape[0]):
        if abs(ev[i]) > 1e-4:
            print("operator {0} with coefficient {1}".format(i, ev[i]))
