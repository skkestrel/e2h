import subprocess
import glob
import os
import sys
import json

def list_servers():
    lines = subprocess.run(['openstack', 'server', 'list'], stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')
    divs = [i for i, char in enumerate(lines[1]) if char == '|']

    if len(divs) != 7:
        raise RuntimeError('unexpected')

    for line in lines[3:-2]:
        cores = int(line[divs[5]+1:divs[6]-1].strip().split('.')[0][1:])
        name = line[divs[1]+1:divs[2]-1].strip()
        status = line[divs[2]+1:divs[3]-1].strip()
        ip = line[divs[3]+1:divs[4]-1].strip().split('=')[1]
        yield (name, status, ip, cores)

servers = list_servers()
cluster_servers = [x for x in servers if x[1] == 'ACTIVE' and x[0].startswith('e2h')]
processes = []

if sys.argv[1] == 'run':
    n_cores = sum(x[3] for x in cluster_servers)
    cur_index = 0

    for server in cluster_servers:
        for i in range(server[3]):
            cmd = 'cd e2h ; nohup python3 e2h.py '
            cmd += ' '.join(sys.argv[2:])
            cmd += ' -threading_index {0} -threading_total {1} -output_file out{2}.json > stdout_{2} 2> stderr{2} &'.format(cur_index, n_cores, i)

            processes.append((server, subprocess.Popen(['ssh', 'ubuntu@{0}'.format(server[2]), cmd],
                    shell=False,
                    stderr=subprocess.PIPE,
                    stdout=subprocess.PIPE)))

            cur_index += 1
elif sys.argv[1] == 'kill':
    cmd = 'killall python3'

    for server in cluster_servers:
        processes.append((server,
                    subprocess.Popen(['ssh', 'ubuntu@{0}'.format(server[2]), cmd],
                    shell=False,
                    stderr=subprocess.PIPE,
                    stdout=subprocess.PIPE)))

elif sys.argv[1] == 'reset':
    cmd = 'rm e2h/std* e2h/out*.json'

    for server in cluster_servers:
        processes.append((server,
                    subprocess.Popen(['ssh', 'ubuntu@{0}'.format(server[2]), cmd],
                    shell=False,
                    stderr=subprocess.PIPE,
                    stdout=subprocess.PIPE)))

elif sys.argv[1] == 'gather':
    try:
        os.mkdir('gather')
    except OSError:
        pass

    files = glob.glob('gather/*')
    for f in files:
        os.remove(f)

    for server in cluster_servers:
        for number in range(server[3]):
            subprocess.run(['scp', 'ubuntu@{0}:e2h/out{1}.json'.format(server[2], number), 'gather'])

elif sys.argv[1] == 'combine':
    combined_data = { 'meas': {} }
    for run in glob.glob('gather/*.json'):
        with open(run, 'r') as f:
            try:
                data = json.load(f)
            except json.JSONDecodeError:
                print('*** could not decode file', run)
                continue

        for key in data['meas']:
            combined_data['meas'][key] = data['meas'][key]
            combined_data['cfg'] = data['cfg']
    with open('combined_data.json', 'w') as f:
        f.write(json.dumps(combined_data, indent=1))

elif sys.argv[1] == 'exec':
    for server in cluster_servers:
        cmd = ' '.join(sys.argv[1:])
        processes.append((server, subprocess.Popen(['ssh', 'ubuntu@{0}'.format(server[2]), cmd],
                shell=False,
                stderr=subprocess.PIPE,
                stdout=subprocess.PIPE)))

for server, proc in processes:
    proc.wait()
    print('***************************************')
    print('output from {0} {1}:'.format(server[0], server[2]))
    print('***************************************')
    for line in (x.decode('utf-8').rstrip('\n') for x in proc.stdout.readlines()):
        print(line)
    for line in (x.decode('utf-8').rstrip('\n') for x in proc.stderr.readlines()):
        print(line)
