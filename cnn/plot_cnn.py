import netket as nk
import json
import numpy as np
import matplotlib.pyplot as plt

# System Size
L = 4
J2 = [0.0, 0.2, 0.4, 0.5, 1.0, 1.5, 2.0, 4.0]
ED_energies = np.array([-44.91393283, -39.67193226, -35.27345745,
                        -33.83169341, -49.18196086, -72.68993439, -96.49331419, -192.232762206])

# Load Graph Data
json_data = open("graph_L4.json").read()
data = json.loads(json_data)

# Create Graph and Hilbert objects
g = nk.graph.CustomGraph(
    data['EdgeColors'], data['Automorphisms'])
hi = nk.hilbert.Spin(s=0.5, total_sz=0.0, graph=g)

# Initialise Network
# CNN network with 3 layers and Log(Cosh(x)) activation function
layers = (
    nk.layer.ConvolutionalHypercube(
        length=L,
        n_dim=2,
        input_channels=1,
        output_channels=6,
        stride=1,
        kernel_length=4,
        use_bias=True,
    ),
    nk.layer.Lncosh(input_size=6 * L * L),
    nk.layer.ConvolutionalHypercube(
        length=L,
        n_dim=2,
        input_channels=6,
        output_channels=4,
        stride=1,
        kernel_length=2,
        use_bias=True,
    ),
    nk.layer.Lncosh(input_size=4 * L * L),
    nk.layer.ConvolutionalHypercube(
        length=L,
        n_dim=2,
        input_channels=4,
        output_channels=2,
        stride=1,
        kernel_length=2,
        use_bias=True,
    ),
    nk.layer.Lncosh(input_size=2 * L * L),
    nk.layer.SumOutput(input_size=(2 * L * L)),
)
ma = nk.machine.FFNN(hi, layers)


cnn_energies = []
cnn_variances = []

for j in J2:
    # Couplings J1 and J2
    J = [1.0, j]

    # Sigma^z*Sigma^z interactions
    sigmaz = [[1, 0], [0, -1]]
    mszsz = (np.kron(sigmaz, sigmaz))

    # Create Hamiltonian
    exchange = np.asarray(
        [[0, 0, 0, 0], [0, 0, 2, 0], [0, 2, 0, 0], [0, 0, 0, 0]])

    # Note the neel/stripe rotation
    # For j2 <= 0.5, we do the standard neel rotation
    # For j2 > 0.5, we rotate alternate rows, i.e. 0,1,2,3 and 8,9,10,11
    # 12 13 14 15
    # 8  9  10 11
    # 4  5  6  7
    # 0  1  2  3
    if j > 0.55:
        bond_operator = [
            (J[0] * mszsz).tolist(),
            (J[0] * mszsz).tolist(),
            (J[1] * mszsz).tolist(),
            (J[0] * exchange).tolist(),
            (-J[0] * exchange).tolist(),
            (-J[1] * exchange).tolist(),
        ]
    else:
        bond_operator = [
            (J[0] * mszsz).tolist(),
            (J[0] * mszsz).tolist(),
            (J[1] * mszsz).tolist(),
            (-J[0] * exchange).tolist(),
            (-J[0] * exchange).tolist(),
            (J[1] * exchange).tolist(),
        ]
    bond_color = [1, 2, 3, 1, 2, 3]
    ha = nk.operator.GraphOperator(
        hi, bondops=bond_operator, bondops_colors=bond_color)
    ham_mat = ha.to_sparse()

    # Load the wavefunction
    # Note that we need to set the wavefunction to zero for the
    # sectors where total magnetization is not zero.
    ma.load('j1_1_j2_' + str(j) + '_cnn_6_4_2.wf')
    wf = []
    for i in range(hi.n_states):
        s = hi.number_to_state(i)
        if (np.abs(np.sum(s)) < 0.5):
            wf.append(np.exp(ma.log_val(s)))
        else:
            wf.append(0)
    wf = np.matrix(wf).transpose()
    wf /= np.linalg.norm(wf)

    # Compute energy Exactly
    energy = np.real(wf.H @ ham_mat @ wf)[0, 0]
    variance = np.real(wf.H @ (ham_mat @ ham_mat) @ wf)[0, 0]
    print('<E> = ', energy)
    cnn_energies.append(energy)
    cnn_variances.append(variance)

cnn_energies = np.array(cnn_energies)
relative_errors = np.abs(np.divide(cnn_energies - ED_energies, ED_energies))

fig, ax = plt.subplots()
#ax.plot(J2, cnn_energies, 'g*', label='CNN')
#ax.plot(J2, ED_energies, 'b--', label='ED')
J2 = np.array(J2)
ax.plot(J2 / (1 + J2), (cnn_energies - ED_energies) / 16, 'g*', label='CNN - ED', markersize=11)
ax.plot(J2 / (1 + J2), np.sqrt(cnn_variances - cnn_energies * cnn_energies) / 16, 'g*', label='variance', markersize=11)

J2_ = np.array([0.1, 0.3, 0.43, 0.46, 0.65, 0.85, 1.5, 3])
ax.scatter(J2_ / (1 + J2_), np.zeros_like(J2_))
# ax.set_xticks(J2)
ax.set_xlabel('$J_2 / (J_1 + J_2)$')
ax.set_ylabel('Energy difference')
# ax.set_ylim(-105, -25)
ax.legend()

"""
ax2 = ax.twinx()
ax2.plot(J2 / (1 + J2), relative_errors, 'ro', label='Relative Error', markersize=9)
ax2.set_xlabel('Relative Error')
ax2.set_yscale('log')
ax2.set_ylim([3e-5, 1e-2])
ax2.legend(loc=1)
plt.tight_layout()
"""
plt.show()
