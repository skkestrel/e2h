import json
import glob
import sys
import config

# this script tells you what command line parameters to use to get the same config as a particular run

def load_run(partial_name, relative_path=False):
    if relative_path:
        with open(partial_name, 'r') as f:
            run_data = json.load(f)
    else:
        matches = glob.glob('runs/' + partial_name.lower() + '*.json')
        if len(matches) > 1:
            print('more than one run with that name')
            sys.exit(1)
        if len(matches) == 0:
            print('no such run exists')
            sys.exit(1)
        with open(matches[0], 'r') as f:
            run_data = json.load(f)

    return run_data['cfg']

cfg = load_run(sys.argv[1], True)
default = config.default()

builder = '{0}x{1} '.format(cfg['geom_len_x'], cfg['geom_len_y'])


for key in default:
    if key in ['geom_len_x', 'geom_len_y']:
        continue

    if default[key] != cfg[key]:
        builder += '-{0} {1} '.format(key, config.repr_option(key, cfg[key]))

print(builder)

